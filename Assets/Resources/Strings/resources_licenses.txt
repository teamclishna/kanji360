License of resources used

The sound effects used by this game is published on the following site:
  The Match makers 2nd
  http://osabisi.sakura.ne.jp/m2/

The musics used by this game is published on the following site:
  HURT RECORD
  https://www.hurtrecord.com/

  meisou.mp3: contributed by SHISHIN
  kore-ha-chikoku-ni-narimasuka.mp3: contributed by OMEGANE
  manin-on-re-i-densha.mp3: contributed by OMEGANE

Font of game characters used "M+ 1p" by M+ FONTS PROJECT.
  https://mplus-fonts.osdn.jp/about.html

The HUD uses Unity's standard Alial font.
Japanese character strings are displayed using the OS font by fallback.
