﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Scenes.Main;
using Assets.Scripts.Types;

namespace Assets.Scripts.Characters
{
    /// <summary>
    /// 敵キャラクタのオブジェクト.
    /// 敵のグラフィックは Create メソッドの引数 spriteImage で区別する.
    /// 敵の種類ごとの動作はこのクラスを継承し、UpdateFrameImpl で実装する.
    /// </summary>
    public class EnemyCharacterObject : AbstractInFieldObject
    {
        public static EnemyCharacterObject Create(
            GameObject template,
            Sprite spriteImage,
            FieldVector position)
        {
            var chara = new EnemyCharacterObject()
            {
                Character = GameObject.Instantiate(
                    template,
                    position.ToVector3(Vector3.zero),
                    Quaternion.FromToRotation(
                        Vector3.down,
                        new FieldVector()
                        {
                            angle = 0.0f,
                            distance = 0.0f
                        }.ToVector3(position.ToVector3(Vector3.zero)))),
                Position = position,
                LookAt = new FieldVector() { angle = 0.0f, distance = 0.0f },
                IsPlayers = false,
            };

            // キャラクタのテクスチャを設定
            chara.Character.GetComponent<SpriteRenderer>().sprite = spriteImage;
            chara.Character.GetComponent<SpriteRenderer>().color = Color.red;

            return chara;
        }

        protected override void UpdateFrameImpl(
            float delta,
            FieldController controller)
        {
            //// とりあえずプレイヤーに向かってゆっくり一直線に近づいてくるようにする
            //this.Position.distance -= (1.0f * delta);

            //// プレイヤーにある程度近づいたら削除
            //// プレイヤーにダメージを与える
            //if(this.Position.distance < 1.0f)
            //{
            //    controller.Player.CurrentHP -= 5.0f;
            //    controller.Parent._soundController.PlayOneshot(SoundId.ENEMY_REACH_LINE);
            //    this.RemoveRequest = true;
            //}
        }

    }
}
