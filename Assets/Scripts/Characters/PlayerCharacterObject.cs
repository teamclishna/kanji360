﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Scenes.Main;
using Assets.Scripts.Skills;

namespace Assets.Scripts.Characters
{
    /// <summary>
    /// プレイヤーが操作する自機のオブジェクト
    /// </summary>
    public class PlayerCharacterObject : AbstractInFieldObject
    {
        #region Constants

        /// <summary>
        /// スキルのインデックス (通常ショット)
        /// </summary>
        public static readonly int SKILL_MAINSHOT = 0;
        /// <summary>
        /// スキルのインデックス (防御)
        /// </summary>
        public static readonly int SKILL_DEFENCE = 1;

        /// <summary>
        /// スキルのインデックス (特殊スキル)
        /// この値 + インデックス値で指定する。
        /// </summary>
        public static readonly int SKILL_SPECIAL = 2;

        /// <summary>
        /// 特殊スキルのチャージの最大値
        /// </summary>
        public static readonly float MAX_SPECIAL_CHARGE = 400.0f;

        #endregion

        #region FIelds

        private float _currentHP;
        private float _maxHP;
        private float _currentCharge;
        private float _recoverCharge;
        private float _specialCharge;

        #endregion

        /// <summary>
        /// 現在のヒットポイント.
        /// </summary>
        public float CurrentHP
        {
            get { return this._currentHP; }
            set
            {
                this._currentHP = value;
                this._currentHP = (
                    this._currentHP < 0.0f ? 0.0f :
                    this._maxHP < this._currentHP ? this._maxHP :
                    this._currentHP);
            }
        }

        /// <summary>
        /// ヒットポイントの最大値
        /// </summary>
        public float MaxHP
        {
            get { return this._maxHP; }
            set { this._maxHP = value; }
        }

        /// <summary>
        /// 現在のチャージ値.
        /// 値域は [0.0f, 1.0f] である.
        /// スキルを使うと消費し、時間経過で回復する
        /// </summary>
        public float CurrentCharge
        {
            get { return this._currentCharge; }
            set
            {
                this._currentCharge = value;
                this._currentCharge = (
                    this._currentCharge < 0.0f ? 0.0f :
                    1.0f < this._currentCharge ? 1.0f :
                    this._currentCharge);
            }
        }

        /// <summary>
        /// 1秒当たりのチャージ値の回復量
        /// </summary>
        public float RecoverCharge
        {
            get { return this._recoverCharge; }
            set
            {
                this._recoverCharge = value;
                this._recoverCharge = (
                    this._recoverCharge < 0.0f ? 1.0f :
                    this._recoverCharge);
            }
        }

        /// <summary>
        /// 特殊スキル用のチャージ値
        /// </summary>
        public float CurrentSpecialChaege
        {
            get { return this._specialCharge; }
            set
            {
                this._specialCharge = value;
                this._specialCharge = (
                    this._specialCharge < 0.0f ? 0.0f :
                    MAX_SPECIAL_CHARGE < this._specialCharge ? MAX_SPECIAL_CHARGE :
                    this._specialCharge
                    );
            }
        }

        /// <summary>
        /// プレイヤーが使えるスキルの一覧
        /// </summary>
        public AbstractSkill[] Skills;

        /// <summary>
        /// 指定したプレハブを複製し、キャラクタのインスタンスを生成して返す.
        /// </summary>
        /// <param name="template">このキャラクタの複製元に使うプレハブ</param>
        /// <param name="spriteImage">このキャラクタに割り当てるスプライト画像</param>
        /// <returns></returns>
        public static PlayerCharacterObject Create(
            GameObject template,
            Sprite spriteImage)
        {
            var chara = new PlayerCharacterObject()
            {
                Character = GameObject.Instantiate(template),
                IsPlayers = true,
            };
            chara.Character.name = "Player";

            // キャラクタのテクスチャを設定
            chara.Character.GetComponent<SpriteRenderer>().sprite = spriteImage;

            return chara;
        }

        /// <summary>
        /// スキルを使用する.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="skillIndex">使用するスキルのインデックス.</param>
        /// <param name="pos"></param>
        public void DoSkill(MainSceneController controller, int skillIndex, Vector3 pos)
        {
            if (skillIndex < this.Skills.Length && this.Skills[skillIndex] != null)
            { this.Skills[skillIndex].Execute(controller, pos); }
        }

        protected override void UpdateFrameImpl(
            float delta,
            FieldController controller)
        {
            var render = this.Character.GetComponent<SpriteRenderer>();

            // スキルのリキャストタイムを更新
            foreach (var skill in this.Skills)
            {
                if (skill != null)
                { skill.UpdateFrame(delta, controller.Parent); }
            }

            // チャージ値の回復
            this.CurrentCharge += (this.RecoverCharge * delta);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PlayerCharacterObject() : base()
        {
            this.Skills = new AbstractSkill[]
            {
                null,
                null,
                null,
                null,
                null,
                null,
            };
        }
    }
}
