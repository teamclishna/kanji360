﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;

using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Characters
{
    /// <summary>
    /// 画面上に表示するラベルオブジェクト
    /// </summary>
    public class LabelObject : AbstractInFieldObject
    {
        /// <summary>
        /// このラベルを表示するスクリーン座標
        /// </summary>
        public Vector3 ScreenPosition;

        /// <summary>
        /// 残りの表示時間
        /// </summary>
        private float _remainShowTime;

        private float _currentShowTime;

        /// <summary>
        /// フィールド上に配置するラベルを生成する
        /// </summary>
        /// <param name="template">インスタンスの複製元にするプレハブ</param>
        /// <param name="showTime">このラベルの表示時間</param>
        /// <param name="labelString">このラベルに表示する文字列</param>
        /// <returns></returns>
        public static LabelObject Create(
            GameObject template,
            float showTime,
            string labelString)
        {
            var lo = GameObject.Instantiate(template);
            var t = lo.GetComponent<Text>();
            t.text = labelString;
            t.material = new Material(t.material.shader);

            // Unity UI として表示するため、座標系を Canvas 配下に設定する
            var c = GameObject.Find("Canvas");
            lo.transform.SetParent(c.transform, false);

            var chara = new LabelObject()
            {
                _remainShowTime = showTime,
                _currentShowTime = 0.0f,
                Character = lo
            };

            return chara;
        }

        protected override void UpdateFrameImpl(
            float delta,
            FieldController controller)
        {
            // Text オブジェクトを Unity UI として表示するためのスクリーン座標で設定しなおす
            var t = this.Character.GetComponent<Text>();
            var c = t.material.color;
            c.a = (1.0f < this._remainShowTime ? 1.0f : this._remainShowTime);
            t.material.color =c;

            this.Character.transform.position = new Vector3(
                this.ScreenPosition.x,
                this.ScreenPosition.y + (float)t.fontSize * this._currentShowTime,
                this.ScreenPosition.z);

            this._remainShowTime -= delta;
            this._currentShowTime += delta;

            // 残り時間が無くなったら消去
            if (this._remainShowTime < 0.0f)
            { this.RemoveRequest = true; }
        }
    }
}
