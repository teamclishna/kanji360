﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Characters
{
    /// <summary>
    /// 敵を貫通する弾丸のオブジェクト
    /// </summary>
    public class PierceBulletCharacterObject : AbstractInFieldObject
    {
        public static PierceBulletCharacterObject Create(
            GameObject template,
            Sprite spriteImage)
        {
            var chara = new PierceBulletCharacterObject()
            {
                Character = GameObject.Instantiate(template)
            };

            // キャラクタのテクスチャを設定
            chara.Character.GetComponent<SpriteRenderer>().sprite = spriteImage;

            return chara;
        }

        protected override void UpdateFrameImpl(
            float delta,
            FieldController controller)
        {
            this.Position.distance += (2.0f * delta);

            // 当たり判定の対象となる敵がいればその敵にダメージを与える
            var bulletSize = this.Character.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f;
            var enemies = controller.FindEnemies(this.Position, bulletSize);
            if (0 < enemies.Count())
            {
                var e = enemies.ElementAt(0);
                e.RemoveRequest = true;

                controller.Parent._soundController.PlayOneshot(SoundId.ENEMY_DEFEAT);
                controller.Parent.DefeatEnemy(this, e);
                return;
            }

            // フィールド外に出たらこのオブジェクトを削除
            var worldPos = this.Position.ToVector3(Vector3.zero);
            if (controller.InField(worldPos))
            { this.RemoveRequest = true; }
        }
    }
}
