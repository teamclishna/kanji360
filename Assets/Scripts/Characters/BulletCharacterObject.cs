﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Characters
{
    /// <summary>
    /// プレイヤー、または敵が発射した弾丸を示すオブジェクト.
    /// </summary>
    public class BulletCharacterObject : AbstractInFieldObject
    {
        public static BulletCharacterObject Create(
            GameObject template,
            Sprite spriteImage)
        {
            var chara = new BulletCharacterObject()
            {
                Character = GameObject.Instantiate(template)
            };

            // キャラクタのテクスチャを設定
            chara.Character.GetComponent<SpriteRenderer>().sprite = spriteImage;

            return chara;
        }

        protected override void UpdateFrameImpl(
            float delta,
            FieldController controller)
        {
            if (this.IsPlayers)
            { UpdatePlayerBullet(delta, controller); }
            else
            { UpdateEnemyBullet(delta, controller); }
        }

        /// <summary>
        /// プレイヤーの弾としての更新
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="controller"></param>
        private void UpdatePlayerBullet(
            float delta,
            FieldController controller)
        {
            this.Position.distance += (5.0f * delta);

            // 当たり判定の対象となる敵がいればその敵にダメージを与える
            // とりあえず即死で
            var bulletSize = this.Character.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f;
            var enemies = controller.FindEnemies(this.Position, bulletSize);
            if (0 < enemies.Count())
            {
                var e = enemies.ElementAt(0);
                e.RemoveRequest = true;
                this.RemoveRequest = true;

                controller.Parent._soundController.PlayOneshot(SoundId.ENEMY_DEFEAT);
                controller.Parent.DefeatEnemy(this, e);
                return;
            }

            // フィールド外に出たらこのオブジェクトを削除
            var worldPos = this.Position.ToVector3(Vector3.zero);
            if (controller.InField(worldPos))
            { this.RemoveRequest = true; }
        }

        /// <summary>
        /// 敵の弾としての更新
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="controller"></param>
        private void UpdateEnemyBullet(
            float delta,
            FieldController controller)
        {
            this.Position.distance -= (1.0f * delta);

            // プレイヤーの陣地に接したらプレイヤーにダメージを与えてこのオブジェクトを削除
            if (this.Position.distance < 1.0f)
            {
                if (0.0f < controller.Damage(this, controller.Player, 0.5f))
                { controller.Parent._soundController.PlayOneshot(SoundId.ENEMY_REACH_LINE); }
                this.RemoveRequest = true;
            }
        }
    }
}
