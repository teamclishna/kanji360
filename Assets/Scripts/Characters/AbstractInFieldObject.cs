﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Types;
using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Characters
{
    /// <summary>
    /// ゲームフィールド上に配置するキャラクタを操作するための抽象クラス
    /// </summary>
    public abstract class AbstractInFieldObject
    {
        /// <summary>
        /// このオブジェクトが操作対象とするGameObject.
        /// 画面上に配置するオブジェクトが存在しない場合 (ビジュアルを持たないDoT系スキルなど) の場合は null
        /// </summary>
        public GameObject Character;

        /// <summary>
        /// キャラクタの削除要求.
        /// フレームの処理終了時にこのフラグがtrueである場合、このキャラクタを削除する.
        /// </summary>
        public bool RemoveRequest;

        /// <summary>
        /// このオブジェクトがプレイヤーのものかどうか.
        /// プレイヤーが発射、配置したオブジェクトであればtrue.
        /// trueの場合、進行方向がスプライト画像の上方向になり、falseの場合下方向になる
        /// </summary>
        public bool IsPlayers;

        /// <summary>
        /// このキャラクタを配置するフィールド座標
        /// </summary>
        public FieldVector Position;

        /// <summary>
        /// このキャラクタの向きを示すフィールド座標
        /// </summary>
        public FieldVector LookAt;

        public void UpdateFrame(float delta, FieldController controller)
        {
            if (this.Character != null)
            {
                // 位置と向きを設定
                var newPosition = Position.ToVector3(Vector3.zero);
                this.Character.transform.position = newPosition;
                this.Character.transform.rotation = Quaternion.FromToRotation(
                    this.IsPlayers ? Vector3.up : Vector3.down,
                    LookAt.ToVector3(newPosition));
            }
            UpdateFrameImpl(delta, controller);
        }

        protected abstract void UpdateFrameImpl(float delta, FieldController controller);
    }
}
