﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace Assets.Scripts.Types
{
    /// <summary>
    /// ゲームフィールド上の座標系を示す構造体.
    /// 画面の中心 (プレイヤーの位置) からの距離とラジアン単位の方角で示す.
    /// 方角は画面上方向 (0.0, 1.0) が 0 で時計回りとする
    /// </summary>
    public struct FieldVector
    {
        /// <summary>画面中心からの距離</summary>
        public float distance;

        /// <summary>方角</summary>
        public float angle;

        /// <summary>
        /// 指定座標を起点にしてこの構造体の示す距離と方角を返す
        /// </summary>
        /// <param name="center"></param>
        /// <returns></returns>
        public Vector3 ToVector3(Vector3 center)
        {
            return new Vector3(
                center.x + (Mathf.Sin(this.angle) * distance),
                center.y + (Mathf.Cos(this.angle) * distance),
                0.0f
                );
        }

        /// <summary>
        /// centerからみたposの位置の距離を方角を計算し、その座標を返す.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static FieldVector ToField(Vector3 center, Vector3 pos)
        {
            return new FieldVector()
            {
                distance = (pos - center).magnitude,
                angle = Mathf.Atan2(pos.x - center.x, pos.y - center.y),
            };
        }
    }
}
