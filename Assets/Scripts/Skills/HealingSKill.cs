﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Characters;
using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Skills
{
    /// <summary>
    /// 時間をかけてHPを回復するためのオブジェクト
    /// </summary>
    public class HealingObject : AbstractInFieldObject
    {
        /// <summary>
        /// 単位時間当たりの回復量
        /// </summary>
        public float HealPerSecond { get; set; }

        /// <summary>
        /// 回復完了までの残り時間
        /// </summary>
        public float RemainHealTime { get; set; }

        protected override void UpdateFrameImpl(float delta, FieldController controller)
        {
            // HPを回復させる
            var p = controller.Player;
            if (0.0f < p.CurrentHP)
            { p.CurrentHP += HealPerSecond * delta; }

            // 回復時間が終了したらフィールドから削除
            this.RemainHealTime -= delta;
            if (this.RemainHealTime <= 0.0f)
            {
                this.RemoveRequest = true;
            }
        }

        public HealingObject() : base()
        {
            this.Character = null;
        }
    }

    /// <summary>
    /// HPを回復するスキル
    /// </summary>
    public class HealingSKill : AbstractSkill, ISpecialSkill
    {
        public override string ICON_SPRITE_NAME { get { return "kanji360_sprites_9"; } }

        public override float RecastTime { get { return 10.0f; } }

        public override float CastTime { get { return 0.0f; } }

        public override float CoolTime { get { return 0.0f; } }

        public override float ChargeDrain { get { return 0.0f; } }

        public int SkillLevel
        { get; set; }

        protected override void ExecuteSkill(MainSceneController controller, Vector3 position)
        {
            var p = controller._field.Player;

            // フィールド上に配置
            controller._field.AddCharacter(new HealingObject()
            {
                RemainHealTime = (10.0f / this.SkillLevel),
                HealPerSecond = p.MaxHP * (this.SkillLevel / 4.0f) / (10.0f / this.SkillLevel),
            });
            controller._soundController.PlayOneshot(SoundId.SKILL_HEAL);
        }
    }
}
