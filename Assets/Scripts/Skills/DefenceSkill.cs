﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Characters;
using Assets.Scripts.Scenes.Main;
using UnityEngine;

namespace Assets.Scripts.Skills
{
    /// <summary>
    /// 防御スキルがトグルで使用中であることを示すオブジェクト.
    /// これ自身は何もしない
    /// </summary>
    public class DefenceSkillObject : AbstractInFieldObject
    {
        protected override void UpdateFrameImpl(float delta, FieldController controller)
        {
            // プレイヤーのチャージを消費する
            var drain = delta * 0.30f;
            var p = controller.Player;
            if(drain < p.CurrentCharge)
            { p.CurrentCharge -= drain; }
            else
            {
                // チャージが不足した場合は強制的にスキル解除
                controller.Parent._soundController.PlayOneshot(SoundId.SKILL_TOGGLE_BREAK);
                this.RemoveRequest = true;
            }
        }

        public DefenceSkillObject() : base()
        {
            this.Character = null;
        }
    }

    /// <summary>
    /// トグルで使用中にチャージを徐々に消費し、消費中はダメージを受けないようになるスキル
    /// </summary>
    public class DefenceSkill : AbstractSkill
    {
        public override string ICON_SPRITE_NAME { get { return "kanji360_sprites_12"; } }

        public override float RecastTime { get { return 0.0f; } }

        public override float CastTime { get { return 0.0f; } }

        public override float CoolTime { get { return 0.0f; } }

        public override float ChargeDrain { get { return 0.0f; } }

        public int SkillLevel
        { get; set; }

        protected override void ExecuteSkill(MainSceneController controller, Vector3 position)
        {
            var p = controller._field.Player;

            // 既にフィールド上にディフェンススキルが配置されているか？
            if (0 < controller._field.FieldObjects.Where(x => x.GetType().Equals(typeof(DefenceSkillObject))).Count())
            {
                // 配置済みのスキルを解除
                var skill = controller._field.FieldObjects.Where(x => x.GetType().Equals(typeof(DefenceSkillObject))).First();
                skill.RemoveRequest = true;
            }
            else
            {
                // フィールド上に配置
                controller._field.AddCharacter(new DefenceSkillObject() { });
                controller._soundController.PlayOneshot(SoundId.SKILL_DEFENCE);
            }
        }
    }
}
