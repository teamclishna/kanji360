﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Skills
{
    /// <summary>
    /// 特殊スキルのインターフェイス
    /// </summary>
    public interface ISpecialSkill
    {
        /// <summary>
        /// スキルのレベル
        /// </summary>
        int SkillLevel { get; set; }
    }
}
