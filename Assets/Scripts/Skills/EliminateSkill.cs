﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

using Assets.Scripts.Scenes.Main;
using Assets.Scripts.Types;

namespace Assets.Scripts.Skills
{
    /// <summary>
    /// 中心近くの敵を全滅させるスキル
    /// </summary>
    public class EliminateSkill : AbstractSkill, ISpecialSkill
    {
        public override string ICON_SPRITE_NAME { get { return "kanji360_sprites_15"; } }

        public override float RecastTime { get { return 60.0f; } }

        public override float CastTime { get { return 0.0f; } }

        public override float CoolTime { get { return 0.0f; } }

        public override float ChargeDrain { get { return 0.0f; } }

        public int SkillLevel
        { get; set; }

        protected override void ExecuteSkill(MainSceneController controller, Vector3 position)
        {
            controller._soundController.PlayOneshot(SoundId.SKILL_ELIMINATE);

            // チャージを全部消費する
            controller._field.Player.CurrentCharge = 0.0f;

            // 中心から一定距離の敵を列挙して削除する
            foreach (var e in controller._field.FindEnemies(new FieldVector() { angle = 0.0f, distance = 0.0f }, 2.5f * SkillLevel))
            {
                e.RemoveRequest = true;
            }
        }
    }
}
