﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Characters;
using Assets.Scripts.Scenes.Main;
using UnityEngine;

namespace Assets.Scripts.Skills
{
    /// <summary>
    /// 真っすぐ飛んでいく弾を撃つスキル
    /// </summary>
    public class NormalBulletSkill : AbstractSkill
    {
        public override string ICON_SPRITE_NAME
        { get { return "kanji360_sprites_1"; } }

        public override float RecastTime { get { return 0.1f; } }

        public override float CastTime { get { return 0.0f; } }

        public override float CoolTime { get { return 0.0f; } }

        public override float ChargeDrain { get { return 0.05f; } }

        /// <summary>弾丸のキャラクタを生成するためのファクトリ</summary>
        public CharacterFactory Factory;

        public PlayerCharacterObject Owner;

        protected override void ExecuteSkill(MainSceneController controller, Vector3 position)
        {
            var bullet = controller._charaFactory.CreateBullet(ICON_SPRITE_NAME, position);
            controller._field.AddCharacter(bullet);

            controller._soundController.PlayOneshot(SoundId.SHOT);
        }

        public NormalBulletSkill() : base()
        { }
    }
}
