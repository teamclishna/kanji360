﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Characters;
using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Skills
{
    /// <summary>
    /// 敵の動きを遅くするためのオブジェクト
    /// </summary>
    public class SlowdownObject : AbstractInFieldObject
    {
        /// <summary>
        /// 敵を遅くしている時間
        /// </summary>
        public float RemainSlowdownTime { get; set; }

        protected override void UpdateFrameImpl(float delta, FieldController controller)
        {
            // 回復時間が終了したらフィールドから削除し、速度を元に戻す
            this.RemainSlowdownTime -= delta;
            if (this.RemainSlowdownTime <= 0.0f)
            {
                controller.EnemyTimescale = 1.0f;
                this.RemoveRequest = true;
            }
        }

        public SlowdownObject() : base()
        {
            this.Character = null;
        }
    }

    /// <summary>
    /// 一定時間的の動きを遅くするスキル
    /// </summary>
    public class SlowdownSkill : AbstractSkill, ISpecialSkill
    {
        public override string ICON_SPRITE_NAME { get { return "kanji360_sprites_10"; } }

        public override float RecastTime { get { return 10.0f; } }

        public override float CastTime { get { return 0.0f; } }

        public override float CoolTime { get { return 0.0f; } }

        public override float ChargeDrain { get { return 0.0f; } }

        public int SkillLevel
        { get; set; }

        protected override void ExecuteSkill(MainSceneController controller, Vector3 position)
        {
            var p = controller._field.Player;

            // フィールド上に配置
            controller._field.AddCharacter(new SlowdownObject()
            {
                RemainSlowdownTime = 5.0f * this.SkillLevel,
            });
            controller._field.EnemyTimescale = 0.25f;
            controller._soundController.PlayOneshot(SoundId.SKILL_SLOW);
        }
    }
}
