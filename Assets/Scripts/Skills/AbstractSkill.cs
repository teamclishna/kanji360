﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Skills
{
    /// <summary>
    /// プレイヤーのスキルを操作するための中小クラス
    /// </summary>
    public abstract class AbstractSkill
    {
        /// <summary>
        /// HUDに表示するこのスキルを示すスプライト名
        /// </summary>
        public abstract string ICON_SPRITE_NAME { get; }

        /// <summary>
        /// リキャストタイム.
        /// スキル発動から次にスキルを使用できるまでの待ち時間.
        /// </summary>
        public abstract float RecastTime { get; }

        /// <summary>
        /// キャストタイム.
        /// 実行ボタンを押してから実際に発動するまでの時間がある場合、このプロパティで発動までの残り時間を示す.
        /// キャスト中に他のスキルを使用した場合、このスキルのキャストはキャンセルされる.
        /// </summary>
        public abstract float CastTime { get; }

        /// <summary>
        /// クールタイム.
        /// クールタイムが 0 になるまで次のスキルは使用できない.
        /// リキャストがこのスキルだけの待ち時間であるのに対し、クールタイムは他のスキルも含めた次の行動までの残り時間.
        /// </summary>
        public abstract float CoolTime { get; }

        /// <summary>
        /// チャージの消費.
        /// スキルを使用する毎に消費するチャージ量.
        /// チャージ量が不足している場合はスキルは不発になる.
        /// </summary>
        public abstract float ChargeDrain { get; }

        /// <summary>
        /// 残りのキャストタイム.
        /// </summary>
        public float RemainCastTime { get; private set; }

        /// <summary>
        /// 残りのリキャストタイム.
        /// </summary>
        public float RemainRecastTime { get; private set; }

        /// <summary>
        /// キャストタイムありのスキルの場合、キャスト開始時に標的した場所の座標.
        /// </summary>
        public Vector3 TargetPosition { get; private set; }

        /// <summary>
        /// フレームの更新
        /// </summary>
        /// <param name="delta"></param>
        /// <returns>この時点でスキルを実行された場合はtrue</returns>
        public bool UpdateFrame(
            float delta,
            MainSceneController controller)
        {
            // スキルのキャスト中
            // 残りのキャストタイムを減算し、0になったら実際にスキルを発動する
            if (0.0f < this.RemainCastTime)
            {
                this.RemainCastTime -= delta;
                if (this.RemainCastTime <= 0.0f)
                {
                    ExecuteSkill(controller, this.TargetPosition);
                    this.RemainCastTime = 0.0f;
                    this.RemainRecastTime = this.RecastTime;
                    return true;
                }
            }

            // 次に発動可能になるまでの残り時間をカウント
            if (0.0f < this.RemainRecastTime)
            {
                this.RemainRecastTime -= delta;
                if (this.RemainRecastTime < 0.0f)
                { this.RemainRecastTime = 0.0f; }
            }

            return false;
        }

        /// <summary>
        /// スキル発動要求.
        /// </summary>
        /// <returns>この時点でスキルを実行された場合はtrue</returns>
        public bool Execute(MainSceneController controller, Vector3 pos)
        {
            // リキャストタイムが残っている場合はスキルを使えない
            if (0.0f < this.RemainRecastTime)
            { return false; }
            else
            {
                var player = controller._field.Player;

                // キャストタイムありの場合でもチャージはこの時点で消費する
                // チャージが足りていない場合は何もせずに終了
                if (player.CurrentCharge < this.ChargeDrain)
                { return false; }
                player.CurrentCharge -= this.ChargeDrain;

                if (0 < this.CastTime)
                {
                    // 発動までのキャストタイムがある場合はキャスト時間を設定する
                    CastSkill(controller, pos);
                    this.RemainCastTime = this.CastTime;
                    return false;
                }
                else
                {
                    // スキルを発動させ、リキャストタイムを設定する.
                    ExecuteSkill(controller, pos);
                    this.RemainRecastTime = this.RecastTime;
                    return true;
                }
            }
        }

        /// <summary>
        /// スキルの発動をキャンセルする.
        /// このスキルがキャスト中ではなかった場合は何もしない.
        /// </summary>
        /// <param name="controller"></param>
        /// <returns>このスキルがキャスト中でキャンセルされた場合は true</returns>
        public bool Cancel(MainSceneController controller)
        {
            if (0 < this.RemainCastTime)
            {
                CancelSkill(controller);
                this.RemainCastTime = 0;
                this.TargetPosition = Vector3.zero;
                return true;
            }
            else
            { return false; }
        }

        /// <summary>
        /// スキルのキャンセル時に呼び出されるコールバック
        /// </summary>
        /// <param name="controller"></param>
        protected virtual void CancelSkill(MainSceneController controller) { }

        /// <summary>
        /// キャストタイムありのスキルの場合、キャスト開始時に呼び出されるコールバック.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="position">キャスト開始時にタップした場所.</param>
        protected virtual void CastSkill(MainSceneController controller, Vector3 position) { }

        /// <summary>
        /// スキルが発動したときのコールバック.
        /// キャストタイムありの場合はキャスト完了時、即時発動の場合はExecuteメソッドの呼び出し時点で即座に呼び出される.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="position">キャスト開始時にタップした場所.</param>
        protected abstract void ExecuteSkill(MainSceneController controller, Vector3 position);
    }
}
