﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace Assets.Scripts.Scenes.Main
{
    /// <summary>
    /// 再生する効果音の種類を特定する列挙子
    /// </summary>
    public enum SoundId
    {
        /// <summary>通常弾発射時</summary>
        SHOT,
        /// <summary>貫通弾発射時</summary>
        SHOT_PIERCE,
        /// <summary>敵が死んだ時</summary>
        ENEMY_DEFEAT,
        /// <summary>敵に陣地まで侵入されたとき</summary>
        ENEMY_REACH_LINE,
        /// <summary>ゲームレベルが上昇した時</summary>
        LEVELUP,
        /// <summary>スキルが使用可能になった時</summary>
        GAUGE_UP,
        /// <summary>プレイヤーが死んだ時</summary>
        PLAYER_DEFEAT,
        /// <summary>防御スキルでダメージを無効化した時</summary>
        DAMAGE_CANCEL,

        /// <summary>スキル使用音(防御)</summary>
        SKILL_DEFENCE,
        /// <summary>スキル使用音(回復)</summary>
        SKILL_HEAL,
        /// <summary>スキル使用音(敵減速)</summary>
        SKILL_SLOW,
        /// <summary>スキル使用音(近距離の敵殲滅)</summary>
        SKILL_ELIMINATE,
        /// <summary>スキル使用音(トグル系スキルの強制解除)</summary>
        SKILL_TOGGLE_BREAK,
    }

    /// <summary>
    /// 効果音をロードして再生するためのオブジェクト
    /// </summary>
    public class SoundController
    {
        private static readonly Dictionary<SoundId, string> SOUND_NAMES = new Dictionary<SoundId, string>()
        {
            {  SoundId.SHOT, "Sounds/gun00" },
            {  SoundId.SHOT_PIERCE, "Sounds/bosu21" },
            {  SoundId.ENEMY_DEFEAT, "Sounds/bosu13" },
            {  SoundId.ENEMY_REACH_LINE, "Sounds/bom01" },
            {  SoundId.DAMAGE_CANCEL, "Sounds/metal25_a" },
            {  SoundId.LEVELUP, "Sounds/power37" },
            {  SoundId.GAUGE_UP, "Sounds/power36" },
            {  SoundId.PLAYER_DEFEAT, "Sounds/bom13" },
            {  SoundId.SKILL_DEFENCE, "Sounds/power36" },
            {  SoundId.SKILL_HEAL, "Sounds/power10" },
            {  SoundId.SKILL_SLOW, "Sounds/byoro00" },
            {  SoundId.SKILL_ELIMINATE, "Sounds/noise20" },
            {  SoundId.SKILL_TOGGLE_BREAK, "Sounds/puu16" },
        };

        /// <summary>
        /// 効果音を再生するためのオーディオソース
        /// </summary>
        private AudioSource _audioSource;

        private Dictionary<SoundId, AudioClip> _sounds;

        private AudioClip _bgm;

        /// <summary>
        /// BGMを再生する
        /// </summary>
        public void PlayBGM()
        {
            this._audioSource.clip = this._bgm;
            this._audioSource.loop = true;
            this._audioSource.Play();
        }

        /// <summary>
        /// 現在再生中のBGMを終了する
        /// </summary>
        public void StopBGM()
        {
            if (this._audioSource.clip != null && this._audioSource.isPlaying)
            { this._audioSource.Stop(); }
        }

        /// <summary>
        /// 指定した効果音を鳴らす
        /// </summary>
        /// <param name="id">効果音のID</param>
        public void PlayOneshot(SoundId id)
        {
            if (this._sounds.ContainsKey(id))
            { this._audioSource.PlayOneShot(this._sounds[id]); }
        }

        public SoundController(MonoBehaviour parent)
        {
            this._audioSource = parent.gameObject.GetComponent<AudioSource>();

            // 効果音のロード
            this._sounds = new Dictionary<SoundId, AudioClip>();
            foreach (var key in SOUND_NAMES.Keys)
            { this._sounds.Add(key, (AudioClip)Resources.Load(SOUND_NAMES[key])); }

            this._bgm = (AudioClip)Resources.Load("Musics/manin-on-re-i-densha");
        }
    }
}
