﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Scenes.Main.Enemies
{
    /// <summary>
    /// プレイヤーの周りを周回しながら接近する敵
    /// </summary>
    public class CirculateEnemy : AbstractEnemy
    {
        /// <summary>フィールド1周分を示すラジアン値</summary>
        private static readonly float CIRCULATE_LENGTH = (float)(2.0 * Math.PI);

        public override bool UpdateFrame(
            float delta,
            FieldController controller,
            int currentLevel)
        {
            // この敵が倒されている場合はここで終了
            if (this.Character.RemoveRequest)
            { return true; }

            // とりあえずプレイヤーに向かってゆっくり一直線に近づいてくるようにする
            this.Character.Position.angle += CIRCULATE_LENGTH * (delta / 16.0f);
            this.Character.Position.distance -= (1.0f * delta);
            if((float)(2.0 * Math.PI) < this.Character.Position.angle)
            { this.Character.Position.angle -= (float)(2.0 * Math.PI); }

            // プレイヤーにある程度近づいたら削除
            // プレイヤーにダメージを与える
            if (this.Character.Position.distance < 1.0f)
            {
                if (0.0f < controller.Damage(this.Character, controller.Player, 5.0f))
                { controller.Parent._soundController.PlayOneshot(SoundId.ENEMY_REACH_LINE); }
                this.Character.RemoveRequest = true;
                return true;
            }

            return false;
        }
    }
}
