﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Scenes.Main;

namespace Assets.Scripts.Scenes.Main.Enemies
{
    /// <summary>
    /// 一定時間ごとに向きを変えながら接近する敵
    /// </summary>
    public class SwayEnemy : AbstractEnemy
    {
        /// <summary>フィールド1周分を示すラジアン値</summary>
        private static readonly float CIRCULATE_LENGTH = (float)(2.0 * Math.PI);

        /// <summary>移動方向</summary>
        public bool Direction;

        /// <summary>移動方向変更のチェックまでの残り時間</summary>
        public float DirectionCheckRemain = 0.0f;

        /// <summary>移動方向変更のチェック間隔</summary>
        public float DirectionCheckInterval = 0.0f;

        public override bool UpdateFrame(
            float delta,
            FieldController controller,
            int currentLevel)
        {
            // この敵が倒されている場合はここで終了
            if (this.Character.RemoveRequest)
            { return true; }

            // 向きを変更する？
            this.DirectionCheckRemain -= delta;
            if(this.DirectionCheckRemain < 0.0f)
            {
                if(UnityEngine.Random.value < 0.5f)
                {
                    // 向きを変更し、プレイヤーに向かって弾を撃つ
                    this.Direction = !this.Direction;

                    var sceneController = controller.Parent;

                    var bullet = sceneController._charaFactory.CreateBullet(
                        "kanji360_sprites_1",
                        this.Character.Position.ToVector3(UnityEngine.Vector3.zero));
                    bullet.Position = this.Character.Position;
                    bullet.IsPlayers = false;
                    sceneController._field.AddCharacter(bullet);
                }
                this.DirectionCheckRemain += this.DirectionCheckInterval;
            }

            // とりあえずプレイヤーに向かってゆっくり一直線に近づいてくるようにする
            this.Character.Position.distance -= (0.25f * delta);
            this.Character.Position.angle += (this.Direction ? 1.0f : -1.0f) * CIRCULATE_LENGTH * (delta / 16.0f);
            if ((float)(2.0 * Math.PI) < this.Character.Position.angle)
            { this.Character.Position.angle -= (float)(2.0 * Math.PI); }
            else if (this.Character.Position.angle < 0.0f)
            { this.Character.Position.angle += (float)(2.0 * Math.PI); }

            // プレイヤーにある程度近づいたら削除
            // プレイヤーにダメージを与える
            if (this.Character.Position.distance < 1.0f)
            {
                if (0.0f < controller.Damage(this.Character, controller.Player, 5.0f))
                { controller.Parent._soundController.PlayOneshot(SoundId.ENEMY_REACH_LINE); }
                this.Character.RemoveRequest = true;
                return true;
            }

            return false;
        }
    }
}
