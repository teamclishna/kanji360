﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenes.Main.Enemies
{
    /// <summary>
    /// 一定距離まで前進した後停止して固定砲台になる敵
    /// </summary>
    public class SnipeEnemy : AbstractEnemy
    {
        /// <summary>
        /// 射撃間隔
        /// </summary>
        public float ShotInterval;

        /// <summary>
        /// 次の射撃までの時間
        /// </summary>
        public float ShotRemainTime;

        public override bool UpdateFrame(
            float delta,
            FieldController controller,
            int currentLevel)
        {
            // この敵が倒されている場合はここで終了
            if (this.Character.RemoveRequest)
            { return true; }

            if (5.0f < this.Character.Position.distance)
            {
                // 一定距離までは前進する
                this.Character.Position.distance -= (5.0f * delta);
            }
            else
            {
                // 一定時間おきに射撃
                this.ShotRemainTime -= delta;
                if(this.ShotRemainTime <= 0.0f)
                {
                    var sceneController = controller.Parent;
                    var bullet = sceneController._charaFactory.CreateBullet(
                        "kanji360_sprites_1",
                        this.Character.Position.ToVector3(UnityEngine.Vector3.zero));
                    bullet.Position = this.Character.Position;
                    bullet.IsPlayers = false;
                    sceneController._field.AddCharacter(bullet);

                    this.ShotRemainTime += this.ShotInterval;
                }
            }
            return false;
        }
    }
}
