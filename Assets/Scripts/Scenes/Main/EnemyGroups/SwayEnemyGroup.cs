﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Scenes.Main.Enemies;

namespace Assets.Scripts.Scenes.Main.EnemyGroups
{
    /// <summary>
    /// 進行方向を変えながら進む敵を生成するグループ
    /// </summary>
    public class SwayEnemyGroup : AbstractEnemyGroup
    {
        /// <summary>この敵のスプライト名</summary>
        private static readonly string SPRITE_NAME = "kanji360_sprites_8";

        /// <summary>
        /// このグループがフィールド上に配置されてからの経過時間
        /// </summary>
        private float TotalTime { get; set; }

        /// <summary>
        /// 前に敵を配置してからの経過時間
        /// </summary>
        private float WaitTime { get; set; }

        public override bool UpdateFrame(
            float delta,
            MainSceneController controller,
            int currentLevel)
        {
            this.TotalTime += delta;
            this.WaitTime += delta;

            // 前の敵を配置してから一定期間経過したら次の敵を配置する
            var maxWaitTime = (
                currentLevel < 20 ? (float)(120 - (currentLevel * 6)) / 60.0f :
                (1.0f / 60.0f));
            if (maxWaitTime < this.WaitTime)
            {
                // 新しい敵を配置する
                var newEnemy = controller._charaFactory.CreateEnemy(
                    SPRITE_NAME,
                    CreateStartPosition());
                controller._field.AddCharacter(newEnemy);

                // この敵を操作するためのオブジェクトを生成
                var newController = new SwayEnemy() {
                    Character = newEnemy,
                    DirectionCheckInterval = 5.0f,
                    DirectionCheckRemain = 5.0f,
                    Direction = true
                };
                controller._field._enemies.Add(newController);

                this.WaitTime -= maxWaitTime;
            }

            // 10秒たったらこのグループの処理終了
            return (10.0f < this.TotalTime);
        }

        /// <summary>
        /// 敵の出現位置を生成する
        /// </summary>
        /// <returns>画面外周のランダムな位置</returns>
        private Types.FieldVector CreateStartPosition()
        {
            return new Types.FieldVector()
            {
                angle = UnityEngine.Random.value * (float)(Math.PI * 2.0f),
                distance = 10.0f
            };
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SwayEnemyGroup()
        {
            this.TotalTime = 0.0f;
            this.WaitTime = 0.0f;
        }
    }
}
