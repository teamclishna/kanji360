﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Scenes.Main.Enemies;

namespace Assets.Scripts.Scenes.Main.EnemyGroups
{
    /// <summary>
    /// フィールドを周回しながらプレイヤーに近づく敵グループ
    /// </summary>
    public class CirculateEnemyGroup : AbstractEnemyGroup
    {
        /// <summary>この敵のスプライト名</summary>
        private static readonly string SPRITE_NAME = "kanji360_sprites_6";

        /// <summary>
        /// 前に敵を配置してからの経過時間
        /// </summary>
        private float WaitTime { get; set; }

        /// <summary>
        /// 配置した敵の数
        /// </summary>
        private int AddEnemies { get; set; }

        /// <summary>
        /// 敵の配置場所
        /// </summary>
        private Types.FieldVector StartPosition { get; set; }

        public override bool UpdateFrame(float delta, MainSceneController controller, int currentLevel)
        {
            this.WaitTime += delta;

            // 前の敵を配置してから一定期間経過したら次の敵を配置する
            // 最初の敵の場合は配置位置を設定する
            if (1.0f < this.WaitTime)
            {
                if (this.StartPosition.distance == 0.0f)
                { this.StartPosition = CreateStartPosition(); }

                // 新しい敵を配置する
                var newEnemy = controller._charaFactory.CreateEnemy(
                    SPRITE_NAME,
                    this.StartPosition);
                controller._field.AddCharacter(newEnemy);

                // この敵を操作するためのオブジェクトを生成
                var newController = new CirculateEnemy() { Character = newEnemy, };
                controller._field._enemies.Add(newController);

                // 既定の数だけ敵を配置したらここで終了
                this.AddEnemies++;
                if (8 <= this.AddEnemies)
                { return true; }
                this.WaitTime -= 1.0f;
            }

            return false;
        }

        /// <summary>
        /// 敵の出現位置を生成する
        /// </summary>
        /// <returns>画面外周のランダムな位置</returns>
        private Types.FieldVector CreateStartPosition()
        {
            return new Types.FieldVector()
            {
                angle = UnityEngine.Random.value * (float)(Math.PI * 2.0f),
                distance = 10.0f
            };
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CirculateEnemyGroup()
        {
            this.WaitTime = 0.0f;
            this.AddEnemies = 0;
            this.StartPosition = new Types.FieldVector() { angle = 0.0f, distance = 0.0f };
        }
    }
}
