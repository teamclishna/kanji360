﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace Assets.Scripts.Scenes.Main
{
    /// <summary>
    /// 今フレームのプレイヤーの操作内容
    /// </summary>
    public enum InputResult
    {
        /// <summary>マウスクリックした</summary>
        TAPPED,
        /// <summary>前フレームから引き続きマウスが押されている</summary>
        PRESS,
        /// <summary>マウスボタンが離された</summary>
        RELEASED,
        /// <summary>マウスボタンは押されていない</summary>
        NOT_TOUCH,
    }

    /// <summary>
    /// フレームごとのプレイヤーの操作内容を返すためのクラス
    /// </summary>
    public class InputData
    {
        /// <summary>マウスボタンの操作情報</summary>
        public InputResult Result;

        /// <summary>マウスカーソルのワールド座標</summary>
        public Vector3 WorldPos;

        /// <summary>マウスカーソルのスクリーン座標</summary>
        public Vector3 ScreenPos;
    }

    /// <summary>
    /// キーボードの入力状態を格納するための構造体
    /// </summary>
    public struct KeyboardInputStruct
    {
        /// <summary>取得対象のキーコード</summary>
        public KeyCode K;
        /// <summary>キーの入力状態</summary>
        public InputResult Result;
    }

    /// <summary>
    /// プレイヤーの操作の検出とマウスカーソル位置の計算を行うためのコントローラオブジェクト
    /// </summary>
    public class InputController
    {
        /// <summary>前フレームにマウスがクリックされていれば true</summary>
        private bool _isPressed;

        /// <summary>スクリーン座標→ワールド座標の変換に使用するカメラオブジェクト</summary>
        public Camera ScreenCamera;

        /// <summary>キーボードの入力状態を格納するためのリスト.</summary>
        private KeyboardInputStruct[] KeyboardInputList;

        /// <summary>
        /// 入力状態の取得対象とするキーの配列.
        /// このプロパティをSetした時点で直前フレームまでの情報は失われる.
        /// </summary>
        public KeyCode[] ScanKeys
        {
            set
            {
                var keys = new KeyboardInputStruct[value.Length];
                for (var cnt = 0; cnt < value.Length; cnt++)
                {
                    keys[cnt] = new KeyboardInputStruct()
                    {
                        K = value[cnt],
                        Result = InputResult.NOT_TOUCH
                    };
                }
                this.KeyboardInputList = keys;
            }
        }

        /// <summary>
        /// マウス操作の情報を返す
        /// </summary>
        /// <returns></returns>
        public InputData GetMouseInput()
        {
            // マウスカーソルのスクリーン座標と、ゲームフィールドのワールド座標に変換した値を取得する
            // ScreenToWorldPoint はカメラ位置の z 座標を返すので
            // ワールド座標は z 座標に 0 を設定しておく
            var screenPosition = Input.mousePosition;
            var worldPosition = this.ScreenCamera.ScreenToWorldPoint(screenPosition);
            worldPosition.z = 0.0f;

            InputResult result;

            if (Input.GetMouseButton(0))
            {

                if (this._isPressed)
                {
                    result = InputResult.PRESS;
                }
                else
                {
                    result = InputResult.TAPPED;
                    this._isPressed = true;
                }
            }
            else
            {
                if (this._isPressed)
                {
                    result = InputResult.RELEASED;
                    this._isPressed = false;
                }
                else
                {
                    result = InputResult.NOT_TOUCH;
                }
            }
            return new InputData()
            {
                Result = result,
                ScreenPos = screenPosition,
                WorldPos = worldPosition,
            };
        }

        /// <summary>
        /// キーボードの状態を取得して返す
        /// </summary>
        /// <returns></returns>
        public KeyboardInputStruct[] GetKeyboardInput()
        {
            for(int cnt = 0; cnt < this.KeyboardInputList.Length; cnt++)
            {
                var ki = this.KeyboardInputList[cnt];
                if (Input.GetKey(ki.K))
                { this.KeyboardInputList[cnt].Result = ((ki.Result == InputResult.TAPPED || ki.Result == InputResult.PRESS) ? InputResult.PRESS : InputResult.TAPPED); }
                else
                { this.KeyboardInputList[cnt].Result = ((ki.Result == InputResult.TAPPED || ki.Result == InputResult.PRESS) ? InputResult.RELEASED : InputResult.NOT_TOUCH); }
            }

            return this.KeyboardInputList;
        }

        public InputController()
        { }
    }

}
