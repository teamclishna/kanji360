﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Characters;
using Assets.Scripts.Types;
using Assets.Scripts.Scenes.Main.EnemyGroups;

namespace Assets.Scripts.Scenes.Main
{
    /// <summary>
    /// ゲームフィールド上のオブジェクトを操作するためのコントローラ
    /// </summary>
    public class FieldController
    {
        /// <summary>敵グループを配置するまでの間隔</summary>
        private static readonly float DEFAULT_INTERVAL_ENEMYGROUP = 5.0f;

        /// <summary>
        /// このコントローラを持つゲームオブジェクト
        /// </summary>
        public MainSceneController Parent;

        /// <summary>
        /// ロガーオブジェクト
        /// </summary>
        public ILogger Logger;

        /// <summary>
        /// ゲームフィールドのワールド座標のサイズ.
        /// ±(x、y) の値がフィールドの左下、右上隅の座標となる。
        /// </summary>
        public Vector3 FieldSize;

        /// <summary>
        /// フィールド上に配置されているオブジェクトを格納するリスト
        /// </summary>
        private List<AbstractInFieldObject> _onFieldObjects;

        /// <summary>
        /// フィールド上に配置されているプレイヤーのオブジェクト
        /// </summary>
        private PlayerCharacterObject _playerObject;

        /// <summary>
        /// フィールド上に敵をグループで配置するためのオブジェクト
        /// </summary>
        private List<AbstractEnemyGroup> _enemyGroups;

        /// <summary>
        /// フィールド上の敵のオブジェクト
        /// </summary>
        public List<AbstractEnemy> _enemies;

        /// <summary>
        /// 次の敵グループを配置するまでの残り時間
        /// </summary>
        private float _remainUntilEnemyGroup;

        /// <summary>
        /// 敵キャラクタのタイムスケール
        /// </summary>
        public float EnemyTimescale;

        /// <summary>
        /// フィールド上にいるオブジェクトのリスト
        /// </summary>
        public List<AbstractInFieldObject> FieldObjects
        { get { return this._onFieldObjects; } }


        /// <summary>
        /// フィールド上に配置するプレイヤーオブジェクト
        /// </summary>
        public PlayerCharacterObject Player {
            get { return this._playerObject; }
            set
            {
                // プレイヤーキャラクタは個別のフィールドとしても管理するが、
                // それとは別に通常キャラクタと同様にUpdate対象のリストにも加える
                // このプロパティを明示的に null に設定する場合、
                // 管理リストからも削除する
                if (value == null)
                {
                    this._onFieldObjects.Remove(this._playerObject);
                    this._playerObject = null;
                }
                else
                {
                    this._playerObject = value;
                    this._onFieldObjects.Add(this._playerObject);
                }
            }
        }

        /// <summary>
        /// ゲーム上のフレームを更新する
        /// </summary>
        /// <param name="delta">前フレームからの経過時間</param>
        public void UpdateFrame(float delta)
        {
            this._remainUntilEnemyGroup += delta;

            // 各キャラクタの時間を進める
            foreach (var o in _onFieldObjects)
            { o.UpdateFrame(delta, this); }

            // 敵キャラクタグループの時間を進める
            if (DEFAULT_INTERVAL_ENEMYGROUP < this._remainUntilEnemyGroup)
            {
                this._enemyGroups.Add(SelectEnemyGroup());
                this._remainUntilEnemyGroup -= DEFAULT_INTERVAL_ENEMYGROUP;
            }

            var itemCount = this._enemyGroups.Count;
            for (var index = itemCount - 1; 0 <= index; index--)
            {
                if (this._enemyGroups[index].UpdateFrame(delta, this.Parent, this.Parent.CurrentGameLevel))
                { this._enemyGroups.RemoveAt(index); }
            }

            // 敵キャラクタは通常のキャラクタとは別に制御する
            itemCount = this._enemies.Count;
            for (var index = itemCount - 1; 0 <= index; index--)
            {
                if (this._enemies[index].UpdateFrame(delta * this.EnemyTimescale, this, this.Parent.CurrentGameLevel))
                { this._enemies.RemoveAt(index); }
            }

            // 削除要求の出ているオブジェクトを取り除く
            // 先にキャラクタの持っているGameObjectをUnityEngineから取り除き、
            // その後でキャラクタ情報をコントローラから削除する
            foreach (var o in this._onFieldObjects.Where(x => x.RemoveRequest))
            {
                UnityEngine.Object.Destroy(o.Character);
                //this.Parent.RemoveCharacter(o);
            }
            this._onFieldObjects.RemoveAll(x => x.RemoveRequest);
        }

        /// <summary>
        /// フィールドにキャラクタを追加する
        /// </summary>
        /// <param name="o">追加するキャラクタ. このキャラクタはプレハブから複製した新規インスタンスを指定する.</param>
        public void AddCharacter(AbstractInFieldObject o)
        { this._onFieldObjects.Add(o); }

        /// <summary>
        /// 指定した座標がゲームフィールド内であるかどうかを判定して返す
        /// </summary>
        /// <param name="pos">座標</param>
        /// <returns>指定した座標がゲームフィールド内であれば true</returns>
        public bool InField(Vector3 pos)
        { return (this.FieldSize.x < Mathf.Abs(pos.x) || this.FieldSize.y < Mathf.Abs(pos.y)); }

        /// <summary>
        /// 指定した座標から指定した距離内に当たり判定のある敵を検索し、列挙して返す
        /// </summary>
        /// <param name="position">検索開始位置</param>
        /// <param name="distance">検索距離</param>
        /// <returns>positionの位置からdistanceの半径内に当たり判定のかかっている敵を格納したList</returns>
        public IEnumerable<EnemyCharacterObject> FindEnemies(FieldVector position, float distance)
        {
            // 起点の座標をVector3に変換する
            var fromPos = position.ToVector3(Vector3.zero);

            var result = new List<EnemyCharacterObject>();
            foreach (var o in this._onFieldObjects)
            {
                var e = o as EnemyCharacterObject;
                if (e == null)
                { continue; }

                var targetPos = e.Position.ToVector3(Vector3.zero);
                // var characterRange = e.Character.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f;
                var positionDistance = Vector3.Distance(fromPos, targetPos);

                if (positionDistance < distance)
                { result.Add(e); }
            }

            return result;
        }

        /// <summary>
        /// 追加する敵グループをランダムに決定する
        /// </summary>
        /// <returns></returns>
        private AbstractEnemyGroup SelectEnemyGroup()
        {
            var currentLevel = this.Parent.CurrentGameLevel;
            if (currentLevel < 5)
            {
                // レベル5以下なら常に直進敵
                return new GoStraightEnemyGroup();
            }
            if (currentLevel < 10)
            {
                // レベル10までは直進敵と周回敵
                return (1.0f < (UnityEngine.Random.value * 2.0f) ? (AbstractEnemyGroup)new GoStraightEnemyGroup() : (AbstractEnemyGroup)new CirculateEnemyGroup());
            }
            if (currentLevel < 15)
            {
                // レベル15までは周回的と回避敵
                return (1.0f < (UnityEngine.Random.value * 2.0f) ? (AbstractEnemyGroup)new SwayEnemyGroup() : (AbstractEnemyGroup)new CirculateEnemyGroup());
            }
            else
            {
                // それ以上はランダム
                var val = UnityEngine.Random.value * 4.0f;
                if (val < 1.0f)
                { return new GoStraightEnemyGroup(); }
                else if (val < 2.0f)
                { return new CirculateEnemyGroup(); }
                else if (val < 3.0f)
                { return new SnipeEnemyGroup(); }
                else
                { return new SwayEnemyGroup(); }
            }
        }

        /// <summary>
        /// キャラクタに対してダメージを与える
        /// </summary>
        /// <param name="source">ダメージソースとなったキャラクタ、または弾丸</param>
        /// <param name="target">攻撃対象となったオブジェクト</param>
        /// <param name="damage">sourceが与えるダメージの基本値</param>
        /// <returns>実際に与えたダメージ値</returns>
        public float Damage(
            AbstractInFieldObject source,
            AbstractInFieldObject target,
            float damage)
        {
            // とりあえずプレイヤーに対してのみダメージ対応を行う
            var p = target as PlayerCharacterObject;
            if (p == null)
            { return 0.0f; }

            // フィールド上に防御オブジェクトが配置されている場合、弾丸からのダメージは受けない
            if (0 < FieldObjects.Where(x => x.GetType().Equals(typeof(Assets.Scripts.Skills.DefenceSkillObject))).Count())
            {
                // ダメージソースは弾丸？
                if ((source as BulletCharacterObject) != null)
                { damage = 0; }
            }

            if (0.0f < damage)
            { p.CurrentHP -= damage; }
            else
            { Parent._soundController.PlayOneshot(SoundId.DAMAGE_CANCEL); }

            return damage;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public FieldController()
        {
            this._onFieldObjects = new List<AbstractInFieldObject>();

            this._enemies = new List<AbstractEnemy>();
            this._enemyGroups = new List<AbstractEnemyGroup>();
            this.EnemyTimescale = 1.0f;

            // ゲーム開始直後に最初の敵グループを登録する
            this._remainUntilEnemyGroup = DEFAULT_INTERVAL_ENEMYGROUP;
        }
    }
}
