﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Characters;

namespace Assets.Scripts.Scenes.Main
{
    /// <summary>
    /// ゲームフィールド上の敵キャラクタを操作するためのオブジェクト
    /// </summary>
    public abstract class AbstractEnemy
    {
        /// <summary>
        /// このオブジェクトで操作するキャラクタ
        /// </summary>
        public EnemyCharacterObject Character;
        
        /// <summary>
        /// フレームごとの処理
        /// </summary>
        /// <param name="delta">前フレームからの経過時間</param>
        /// <param name="controller">この敵を配置したフィールドのコントローラ</param>
        /// <param name="currentLevel">現在のゲームレベル</param>
        /// <returns>この処理が終了した時点でこの敵をフィールドから削除する場合は true</returns>
        abstract public bool UpdateFrame(
            float delta,
            FieldController controller,
            int currentLevel);
    }
}
