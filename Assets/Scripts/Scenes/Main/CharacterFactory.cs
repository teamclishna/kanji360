﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Characters;
using Assets.Scripts.Skills;
using Assets.Scripts.Types;

namespace Assets.Scripts.Scenes.Main
{
    /// <summary>
    /// フィールド内に配置するキャラクタを複製するためのファクトリオブジェクト
    /// </summary>
    public class CharacterFactory
    {
        public ILogger Logger;

        /// <summary>
        /// キャラクタのスプライト画像の配列
        /// </summary>
        private Sprite[] _spriteImages;

        /// <summary>
        /// 自機キャラクタの複製元とするプレハブ
        /// </summary>
        private GameObject _playerObject;

        /// <summary>
        /// フィールド上のキャラクタの複製元とするプレハブ
        /// </summary>
        private GameObject _characterObject;

        /// <summary>
        /// キャラクタ (プレイヤー・敵とも) が発射する弾の複製元とするプレハブ
        /// </summary>
        private GameObject _bulleltObject;

        /// <summary>
        /// フィールド上に配置する敵キャラクタの複製元とするプレハブ
        /// </summary>
        private GameObject _enemyObject;

        /// <summary>
        /// 敵を倒したときなどに表示するラベルの複製元とするプレハブ
        /// </summary>
        private GameObject _scoreIndicatorLabelObject;

        /// <summary>
        /// ファクトリの初期化を行う
        /// </summary>
        public CharacterFactory Init()
        {
            // キャラクタの複製元として使うプレハブのロード
            this._playerObject = (GameObject)Resources.Load("Sprites/PlayerCharaPrefab");
            this._characterObject = (GameObject)Resources.Load("Sprites/PlayerCharaPrefab");
            this._bulleltObject = (GameObject)Resources.Load("Sprites/BulletPrefab");
            this._enemyObject = (GameObject)Resources.Load("Sprites/EnemyCharaPrefab");

            this._scoreIndicatorLabelObject = (GameObject)Resources.Load("Sprites/ScoreIndicateLabel");

            this._spriteImages = Resources.LoadAll<Sprite>("Textures/kanji360_sprites");

            return this;
        }

        /// <summary>
        /// プレイヤーキャラクタを生成して返す
        /// </summary>
        /// <returns></returns>
        public PlayerCharacterObject CreatePlayerCharacter()
        {
            var chara = PlayerCharacterObject.Create(this._playerObject, GetSpriteImage("kanji360_sprites_0"));

            chara.MaxHP = 100.0f;
            chara.CurrentHP = 100.0f;
            chara.RecoverCharge = 0.25f;
            chara.CurrentCharge = 1.0f;

            // プレイヤーは画面の中心に配置する
            chara.Position = new FieldVector()
            {
                angle = 0.0f,
                distance = 0.0f,
            };

            return chara;
        }

        /// <summary>
        /// 敵キャラクタを生成して返す
        /// </summary>
        /// <param name="spriteName"></param>
        /// <param name="position">敵の初期位置</param>
        /// <returns></returns>
        public EnemyCharacterObject CreateEnemy(string spriteName, FieldVector position)
        {
            return EnemyCharacterObject.Create(
                this._enemyObject,
                GetSpriteImage(spriteName),
                position);
        }

        /// <summary>
        /// 弾のオブジェクトを生成して返す
        /// </summary>
        /// <param name="characterId">生成するキャラクタのID</param>
        /// <param name="position">キャラクタを配置する場所</param>
        /// <returns></returns>
        public AbstractInFieldObject CreateBullet(string characterId, Vector3 position)
        {
            var angle = FieldVector.ToField(Vector3.zero, position);
            //var bullet = BulletCharacterObject.Create(
            //    this._bulleltObject,
            //    GetSpriteImage("kanji360_sprites_1"));
            var bullet = CreateBullet(characterId);
            if (bullet != null)
            {

                bullet.Position = new FieldVector()
                {
                    distance = 0.0f,
                    angle = angle.angle,
                };

                bullet.LookAt = bullet.Position;
                bullet.IsPlayers = true;
            }
            return bullet;
        }

        /// <summary>
        /// 弾丸のオブジェクトを生成して返す
        /// </summary>
        /// <param name="characterID"></param>
        /// <returns></returns>
        private AbstractInFieldObject CreateBullet(string characterID)
        {
            switch (characterID)
            {
                case "kanji360_sprites_1":
                    return BulletCharacterObject.Create(this._bulleltObject, GetSpriteImage(characterID));
                case "kanji360_sprites_2":
                    return PierceBulletCharacterObject.Create(this._bulleltObject, GetSpriteImage(characterID));
                default:
                    return null;
            }
        }

        /// <summary>
        /// スコア表示などのためのラベルオブジェクトを生成して返す
        /// </summary>
        /// <param name="labelString">表示するラベル文字列</param>
        /// <param name="screenPosition">表示位置. Canvas 上に配置するのでスクリーン座標系で指定する</param>
        /// <returns></returns>
        public AbstractInFieldObject CreateLabel(string labelString, Vector3 screenPosition)
        {
            var angle = FieldVector.ToField(Vector3.zero, screenPosition);
            var label = LabelObject.Create(
                this._scoreIndicatorLabelObject,
                3.0f,
                labelString);

            label.ScreenPosition = screenPosition;
            label.IsPlayers = true;

            return label;
        }

        /// <summary>
        /// スコア表示などのためのラベルオブジェクトを生成して返す
        /// </summary>
        /// <param name="labelString">表示するラベル文字列</param>
        /// <param name="fv">表示位置</param>
        /// <returns></returns>
        public AbstractInFieldObject CreateLabel(string labelString, FieldVector fv)
        { return CreateLabel(labelString, fv.ToVector3(Vector3.zero)); }

        /// <summary>
        /// 指定した識別子を持つスプライト画像を返す
        /// </summary>
        /// <param name="spriteID">取得するスプライトの識別子</param>
        /// <returns>指定した識別子に該当するスプライト画像</returns>
        public Sprite GetSpriteImage(string spriteID)
        { return this._spriteImages.Where(x => x.name.Equals(spriteID)).First(); }

        public CharacterFactory()
        { }
    }
}
