﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Characters;
using Assets.Scripts.Skills;

namespace Assets.Scripts.Scenes.Main
{
    /// <summary>
    /// メインシーン起動時に渡すパラメータ.
    /// シーン間で渡すいい方法を思いつかなかったのでシングルトンにして渡してみる
    /// </summary>
    public class MainSceneStartParameter
    {
        /// <summary>
        /// シーン間を跨って保持するインスタンス
        /// </summary>
        public static MainSceneStartParameter _instance;

        /// <summary>
        /// プレイヤーのスキル
        /// </summary>
        public AbstractSkill[] StartupSkills { get; set; }

        /// <summary>
        /// インスタンス
        /// </summary>
        public static MainSceneStartParameter Instance
        {
            get { return MainSceneStartParameter._instance; }
            set { MainSceneStartParameter._instance = value; }
        }
    }
}
