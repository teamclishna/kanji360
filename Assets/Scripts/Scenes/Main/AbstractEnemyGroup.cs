﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.Scripts.Scenes.Main;
using Assets.Scripts.Types;

namespace Assets.Scripts.Scenes.Main
{
    /// <summary>
    /// 敵グループを画面上に配置するためのオブジェクト
    /// </summary>
    public abstract class AbstractEnemyGroup
    {
        /// <summary>
        /// 前に敵を配置してからの経過時間
        /// </summary>
        private float WaitTime { get; set; }

        /// <summary>
        /// フレームごとの処理
        /// </summary>
        /// <param name="delta">前フレームからの経過時間</param>
        /// <param name="controller">呼び出し元のコントローラオブジェクト</param>
        /// <param name="currentLevel">現在のゲームレベル</param>
        /// <returns>このグループの処理が終了した場合は true</returns>
        abstract public bool UpdateFrame(float delta, MainSceneController controller, int currentLevel);
    }
}
