﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Assets.Scripts.Characters;
using Assets.Scripts.Scenes.Main;
using Assets.Scripts.Skills;
using Assets.Scripts.Types;

/// <summary>
/// メインシーンのコントローラオブジェクト
/// </summary>
public class MainSceneController : MonoBehaviour {

    /// <summary>
    /// 画面上にラベルを配置するための情報
    /// </summary>
    private struct LabelQueueStruct
    {
        /// <summary>表示するラベル</summary>
        public string LabelString;
        /// <summary>表示位置のスクリーン座標</summary>
        public Vector3 Position;
    }

    /// <summary>
    /// ハイスコア情報
    /// </summary>
    private struct HiscoreStruct
    {
        /// <summary></summary>
        public DateTime ScoreDateTime;
        /// <summary></summary>
        public ulong Score;
        /// <summary></summary>
        public float AliveTime;
    }

    /// <summary>
    /// ハイスコア保存時の日付/時刻の書式文字列
    /// </summary>
    private static readonly string FORMAT_HISCORE_DATETIME = "yyyy/MM/dd HH:mm:ss";

    public ILogger Logger;

    public Camera MainCamera;

    public GameObject LifeGaugeObject;

    public GameObject ChargeGaugeObject;

    public GameObject DefenceButton;

    public List<GameObject> SkillButtonList;

    /// <summary>
    /// プレイヤーの陣地を表示するための Cylinder オブジェクト.
    /// デザイナ上で x 軸中心に 90 度回転させておく (円の断面が見える角度にしておく)
    /// </summary>
    public GameObject MyBaseObject;

    #region UI Objects

    public GameObject GameTimeLabelObject;
    public GameObject ScoreLabelObject;
    public GameObject CurrentLevelLabelObject;
    public GameObject ComboLabelObject;
    public GameObject ComboRemainLabelObject;
    public GameObject SpecialChargeLabelObject;

    public GameObject YouDiedLabelObject;
    public GameObject RankingLabelObject;
    public GameObject GotoTitleButtonObject;

    public GameObject FadePanelObject;

    #endregion

    /// <summary>
    /// フィールド上のオブジェクトを操作するためのコントローラ
    /// </summary>
    public FieldController _field { get; private set; }

    /// <summary>
    /// プレイヤーの操作内容を取得するためのコントローラ
    /// </summary>
    public InputController _inputController { get; private set; }

    /// <summary>
    /// 効果音を再生するためのコントローラ
    /// </summary>
    public SoundController _soundController { get; private set; }

    /// <summary>
    /// キャラクタをプレハブから複製するためのファクトリオブジェクト
    /// </summary>
    public CharacterFactory _charaFactory { get; private set; }

    /// <summary>
    /// 画面サイズの + 方向のサイズを示すワールド座標
    /// </summary>
    public Vector3 ScreenSizeRect { get; private set; }

    /// <summary>
    /// 現在のスコア
    /// </summary>
    private ulong Score { get; set; }

    /// <summary>
    /// ゲーム時間
    /// </summary>
    private float GameTime { get; set; }

    /// <summary>
    /// 現在のコンボ数
    /// </summary>
    private int Combo { get; set; }

    /// <summary>
    /// コンボの残り時間
    /// </summary>
    private float RemainComboTime { get; set; }

    /// <summary>
    /// 現在のゲームレベル
    /// </summary>
    public int CurrentGameLevel { get; set; }

    /// <summary>
    /// 現在のゲームレベルで敵を倒した数.
    /// </summary>
    public int DefeatCount { get; set; }

    /// <summary>
    /// ゲーム終了？
    /// </summary>
    public bool GameFinished { get; set; }

    /// <summary>画面上にラベルを配置するための配置要求リスト</summary>
    private List<LabelQueueStruct> _labelQueue = new List<LabelQueueStruct>();

    /// <summary>ゲームオーバー時にパネルを使用して画面を暗転させるためのα値</summary>
    private float fadePanelAlpha = 0.0f;

	// Use this for initialization
	void Start () {

        this.GameFinished = false;

        // 画面サイズを取得してワールド座標の範囲に変換する
        this.ScreenSizeRect = this.MainCamera.ScreenToWorldPoint(
            new Vector3(
                Screen.width,
                Screen.height,
                0.0f));

        // コントローラを生成
        this._field = new FieldController()
        {
            Logger = this.Logger,
            FieldSize = this.ScreenSizeRect,
            Parent = this,
        };

        this._inputController = new InputController()
        {
            ScreenCamera = this.MainCamera,
            ScanKeys = new KeyCode[] {
                KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4,
                KeyCode.Keypad1, KeyCode.Keypad2, KeyCode.Keypad3, KeyCode.Keypad4,
                KeyCode.Alpha0, KeyCode.Keypad0,
                KeyCode.K,
            },
        };

        this._soundController = new SoundController(this);

        this._charaFactory = new CharacterFactory()
        {
            Logger = this.Logger,
        }.Init();

        // キャラクタを生成し、フィールド内に設置する
        this._field.Player = InitPlayer();

        // 陣地のサイズを設定
        // とりあえず今は半径 1.0 に設定する
        this.MyBaseObject.transform.localScale = new Vector3(2.0f, 0.1f, 2.0f);

        // ゲーム情報の初期化
        this.Score = 0;
        this.Combo = 0;
        this.GameTime = 0.0f;
        this.RemainComboTime = 0.0f;
        this.CurrentGameLevel = 1;
        this.DefeatCount = 0;

        // HUDのゲージを設定
        SetHUD();
        SetGameOverScreen();

        this._soundController.PlayBGM();
    }

    // Update is called once per frame
    void Update()
    {
        var delta = Time.deltaTime;

        if (this.GameFinished)
        {
            // ゲームオーバー時、パネルのα値を設定して暗転処理を行う
            if (fadePanelAlpha < 0.75f)
            {
                this.fadePanelAlpha += (0.75f / 5.00f) * delta;
                var img = this.FadePanelObject.GetComponent<Image>();
                img.color = new Color(1.0f, 0.0f, 0.0f, this.fadePanelAlpha);
            }
            return;
        }

        this.GameTime += delta;

        // 時間経過によるスコア加算
        this.Score += (ulong)(delta * 1000);

        // コンボの残り時間を減算
        if (0.0f < this.RemainComboTime)
        {
            this.RemainComboTime -= delta;
            if (this.RemainComboTime <= 0.0f)
            {
                this.Combo = 0;
                this.RemainComboTime = 0.0f;
            }
        }

        this._labelQueue.Clear();

        // ゲームフィールドの時間を進める
        this._field.UpdateFrame(delta);

        // マウス操作の情報を取得
        var inputData = this._inputController.GetMouseInput();

        switch (inputData.Result)
        {
            case InputResult.TAPPED:
                OnDown(inputData.WorldPos);
                break;

            case InputResult.PRESS:
                OnHold(inputData.WorldPos);
                break;

            case InputResult.RELEASED:
                OnUp(inputData.WorldPos);
                break;
        }

        // キーボード操作の情報を取得
        var keys = this._inputController.GetKeyboardInput();
        CheckUseSkill(keys);

        // 自爆キーが押されたら即座に終了
        foreach (var k in keys)
        {
            if (k.K == KeyCode.K && k.Result == InputResult.TAPPED)
            { this._field.Player.CurrentHP = 0.0f; }
        }

        // フィールド上に配置要求が出ていたラベルの配置
        AddFieldLabel();

        // HUD更新
        UpdateHUD();

        // スキルボタン更新
        EnableSkillButtons(delta);

        // ゲーム終了判定
        // プレイヤーが死んだ？
        if (this._field.Player.CurrentHP == 0.0f)
        {
            this._soundController.PlayOneshot(SoundId.PLAYER_DEFEAT);
            this.GameFinished = true;
            ShowGameoverScreen();
        }
    }

    /// <summary>
    /// スキル使用のボタンを押しているかどうかの判定
    /// </summary>
    private void CheckUseSkill(KeyboardInputStruct[] kInput)
    {
        var skillKeys = new KeyCode[][] {
            new KeyCode[]{ KeyCode.Alpha1, KeyCode.Keypad1},
            new KeyCode[]{ KeyCode.Alpha2, KeyCode.Keypad2},
            new KeyCode[]{ KeyCode.Alpha3, KeyCode.Keypad3},
            new KeyCode[]{ KeyCode.Alpha4, KeyCode.Keypad4},
        };

        var defenceSkillKeys = new KeyCode[][]
        {
            new KeyCode[]{ KeyCode.Alpha0, KeyCode.Keypad0 },
        };

        // スキル使用のキーに一致したものがあるか？
        for (int index = 0; index < skillKeys.Length; index++)
        {
            foreach (var sk in skillKeys[index])
            {
                foreach(var k in kInput)
                {
                    if(k.K == sk && k.Result == InputResult.TAPPED)
                    {
                        UseSkill(index);
                    }
                }
            }
        }

        for (int index = 0; index < defenceSkillKeys.Length; index++)
        {
            foreach (var sk in defenceSkillKeys[index])
            {
                foreach (var k in kInput)
                {
                    if (k.K == sk && k.Result == InputResult.TAPPED)
                    { UseDefenceSkill(); }
                }
            }
        }
    }

    /// <summary>
    /// 防御スキルを使用する
    /// </summary>
    private void UseDefenceSkill()
    {
        var p = this._field.Player;
        var skill = p.Skills[PlayerCharacterObject.SKILL_DEFENCE];

        if (skill != null)
        {
            p.DoSkill(this, PlayerCharacterObject.SKILL_DEFENCE, Vector3.zero);
        }
    }

    /// <summary>
    /// 特殊スキルを使用する
    /// </summary>
    /// <param name="skillIndex">使用するスキルのインデックス。先頭は0</param>
    private void UseSkill(int skillIndex)
    {
        var p = this._field.Player;
        var skill = p.Skills[PlayerCharacterObject.SKILL_SPECIAL + skillIndex];

        if (skill != null)
        {
            if ((skillIndex + 1) * 100.0f <= p.CurrentSpecialChaege && skill.RemainRecastTime == 0.0f)
            {
                // スキル使用
                p.CurrentSpecialChaege -= (skillIndex + 1) * 100.0f;
                p.DoSkill(this, PlayerCharacterObject.SKILL_SPECIAL + skillIndex, Vector3.zero);
            }
            else
            { this._soundController.PlayOneshot(SoundId.ENEMY_DEFEAT); }
        }
        else
        {
            // スキルがセットされていない場合は無視
        }
    }

    /// <summary>
    /// マウスボタン押下時のイベント
    /// </summary>
    /// <param name="mousePosition"></param>
    /// <returns>何らかのオブジェクトが応答した場合trueを返す</returns>
    private bool OnDown(Vector3 mousePosition)
    {
        //Logger.Log(string.Format("ボタン押した: ({0}, {1})", mousePosition.x, mousePosition.y));

        // タップ位置とスキルボタンが重なった場合、そのスキルを使う
        {
            var btn = DefenceButton;
            var x = Mathf.Abs(mousePosition.x - btn.transform.position.x);
            var y = Mathf.Abs(mousePosition.y - btn.transform.position.y);
            if (x < 1.0f && y < 1.0f)
            {
                UseDefenceSkill();
                return true;
            }
        }

        for (var index = 0; index < SkillButtonList.Count; index++)
        {
            var btn = SkillButtonList[index];
            var x = Mathf.Abs(mousePosition.x - btn.transform.position.x);
            var y = Mathf.Abs(mousePosition.y - btn.transform.position.y);
            if (x < 1.0f && y < 1.0f)
            {
                UseSkill(index);
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// マウスボタン押下中のイベント
    /// OnDown がボタンを押した瞬間に一度だけ呼ばれるのに対して
    /// OnHold は押している間呼ばれ続ける
    /// </summary>
    /// <param name="mousePosition"></param>
    private void OnHold(Vector3 mousePosition)
    {
        var player = this._field.Player;

        // スキルを使用する
        player.DoSkill(this, PlayerCharacterObject.SKILL_MAINSHOT, mousePosition);

        // マウスカーソルのある方向に向く
        player.LookAt = FieldVector.ToField(Vector3.zero, mousePosition);
    }

    /// <summary>
    /// マウスボタンを離したときに呼ばれるイベント
    /// </summary>
    /// <param name="mousePosition"></param>
    private void OnUp(Vector3 mousePosition)
    {
        //Logger.Log(string.Format("ボタン離した: ({0}, {1})", mousePosition.x, mousePosition.y));
    }

    /// <summary>
    /// ゲームオーバー時のタイトル画面に戻るボタンを押したときのイベント
    /// </summary>
    public void OnGotoTitleButton()
    { GotoTitleScene(); }

    /// <summary>
    /// プレイヤーの情報を初期化して返す
    /// </summary>
    /// <returns></returns>
    private PlayerCharacterObject InitPlayer()
    {
        var player = this._charaFactory.CreatePlayerCharacter();
        var sceneArgs = MainSceneStartParameter.Instance;

        player.Skills[PlayerCharacterObject.SKILL_MAINSHOT] = System.Activator.CreateInstance(sceneArgs.StartupSkills[PlayerCharacterObject.SKILL_MAINSHOT].GetType()) as AbstractSkill;
        player.Skills[PlayerCharacterObject.SKILL_DEFENCE] = System.Activator.CreateInstance(sceneArgs.StartupSkills[PlayerCharacterObject.SKILL_DEFENCE].GetType()) as AbstractSkill;

        if (sceneArgs != null)
        {
            for (int cnt = 0; cnt < 4; cnt++)
            {
                if (sceneArgs.StartupSkills[PlayerCharacterObject.SKILL_SPECIAL + cnt] != null)
                {
                    var skill = System.Activator.CreateInstance(sceneArgs.StartupSkills[PlayerCharacterObject.SKILL_SPECIAL + cnt].GetType()) as AbstractSkill;
                    (skill as ISpecialSkill).SkillLevel = cnt + 1;
                    player.Skills[PlayerCharacterObject.SKILL_SPECIAL + cnt] = skill;
                }
            }
        }
        else
        {
            // 他シーンからの遷移ではない (エディタから直接起動した) 場合は
            // 適当な初期値を与える
            player.Skills[PlayerCharacterObject.SKILL_SPECIAL + 0] = new HealingSKill() { SkillLevel = 1, };
            player.Skills[PlayerCharacterObject.SKILL_SPECIAL + 1] = new SlowdownSkill() { SkillLevel = 2, };
        }

        return player;
    }

    /// <summary>
    /// ゲーム画面に表示するゲージ類の初期位置を設定する
    /// </summary>
    private void SetHUD()
    {
        var fieldSize = this._field.FieldSize;

        // 画面上端、中央にスコア
        // 右上角から下方向に経過時間、コンボの残り時間
        // 左上角に下方向にレベル

        // スコア
        var objectSize = this.ScoreLabelObject.GetComponent<RectTransform>().sizeDelta;
        this.ScoreLabelObject.transform.position = new Vector3(
            Screen.width / 2.0f,
            Screen.height - (objectSize.y * 0.5f),
            -1.0f);
        UpdateLabel(this.ScoreLabelObject, string.Format("{0}", this.Score));

        //ゲームレベル
        objectSize = this.CurrentLevelLabelObject.GetComponent<RectTransform>().sizeDelta;
        this.CurrentLevelLabelObject.transform.position = new Vector3(
            objectSize.x / 2.0f,
            Screen.height - objectSize.y,
            -1.0f);
        UpdateLabel(
            this.CurrentLevelLabelObject,
            string.Format(
                "Lv. {0} ({1} remain.)",
                this.CurrentGameLevel,
                ((this.CurrentGameLevel * 10) - this.DefeatCount)));

        // スキルのチャージ
        //objectSize = this.SpecialChargeLabelObject.transform.localScale;
        //this.SpecialChargeLabelObject.transform.position = new Vector3(
        //    Screen.width * (1.0f / 4.0f),
        //    Screen.height - 32.0f,
        //    -1.0f);
        this.SpecialChargeLabelObject.SetActive(false);

        // ゲーム開始からの経過時間
        objectSize = this.GameTimeLabelObject.GetComponent<RectTransform>().sizeDelta;
        this.GameTimeLabelObject.transform.position = new Vector3(
            Screen.width - (objectSize.x / 2.0f),
            Screen.height - (objectSize.y * 1.0f),
            -1.0f);
        UpdateLabel(this.GameTimeLabelObject, string.Format("{0:0.00}", this.GameTime));

        // コンボ (コンボ残り時間)
        var objectSizeComboTime = this.ComboRemainLabelObject.GetComponent<RectTransform>().sizeDelta;
        this.ComboRemainLabelObject.transform.position = new Vector3(
            Screen.width - (objectSizeComboTime.x / 2.0f),
            Screen.height - (objectSizeComboTime.y * 2.0f),
            -1.0f);
        UpdateLabel(this.ComboRemainLabelObject, "");

        // コンボ (コンボ数)
        var objectSizeComboCount = this.ComboLabelObject.GetComponent<RectTransform>().sizeDelta;
        this.ComboLabelObject.transform.position = new Vector3(
            Screen.width - objectSizeComboTime.x - (objectSizeComboCount.x / 2.0f),
            Screen.height - (objectSizeComboCount.y * 2.0f),
            -1.0f);
        UpdateLabel(this.ComboLabelObject, "");

        // 体力ゲージとチャージゲージ
        objectSize = this.LifeGaugeObject.transform.localScale;
        this.LifeGaugeObject.transform.position = new Vector3(
            0.0f,
            -fieldSize.y + (objectSize.y * 0.5f),
            -1.0f);
        UpdateGauge(this.LifeGaugeObject, 1.0f);

        objectSize = this.ChargeGaugeObject.transform.localScale;
        this.ChargeGaugeObject.transform.position = new Vector3(
            0.0f,
            -fieldSize.y + (objectSize.y * 1.5f),
            -1.0f);

        // 特殊スキルボタン
        CreateSkillButtons();

        // 初期状態でHUDを表示
        UpdateGauge(this.ChargeGaugeObject, 1.0f);
    }

    /// <summary>
    /// ゲームオーバー時に表示するラベルを設定する.
    /// この時点では非表示状態にしておく
    /// </summary>
    private void SetGameOverScreen()
    {
        var youDiedLabel = YouDiedLabelObject.GetComponent<Text>();
        youDiedLabel.transform.position = new Vector3(
            Screen.width / 2.0f,
            Screen.height * 0.875f,
            -1.0f);

        var rankingLabel = RankingLabelObject.GetComponent<Text>();
        var textSize = rankingLabel.GetComponent<RectTransform>();
        rankingLabel.transform.position = new Vector3(
            Screen.width * 0.5f,
            Screen.height * 0.7f - (textSize.sizeDelta.y * 0.5f),
            -1.0f);

        var gotoTitleButton = GotoTitleButtonObject.GetComponent<Button>();
        gotoTitleButton.transform.position = new Vector3(
            Screen.width / 2.0f,
            Screen.height * 0.125f,
            -1.0f);

        // タイトル画面に戻るボタンにイベントを設定しておく
        GotoTitleButtonObject.GetComponent<Button>().onClick.AddListener(OnGotoTitleButton);

        YouDiedLabelObject.SetActive(false);
        RankingLabelObject.SetActive(false);
        GotoTitleButtonObject.SetActive(false);
    }

    /// <summary>
    /// ゲームオーバー時に表示するハイスコアを表示
    /// </summary>
    private void ShowGameoverScreen()
    {
        // 新しいハイスコアを取得
        var highScoreList = SaveHighscore(DateTime.Now, this.Score, this.GameTime);
        // 今回のランクを取得
        var currentRank = highScoreList
            .Select((item, index) => new { Index = index, Value = item })
            .Where(x => x.Value.Score == this.Score)
            .Select(x => x.Index);

        var sb = new StringBuilder();
        for (int cnt = 0; cnt < 10; cnt++)
        {
            if (highScoreList.Count <= cnt)
            { break; }

            var playTime = (highScoreList[cnt].AliveTime < 60.0f ?
                string.Format("{0:0.00}s", (highScoreList[cnt].AliveTime % 60.0f)) :
                string.Format("{0,4}m {1:0.00}s", (int)(highScoreList[cnt].AliveTime / 60.0f), (highScoreList[cnt].AliveTime % 60.0f)));
            sb.Append(
                string.Format(
                    "{0,2} {1:#,0} {2} {3:yyyy/MM/dd HH:mm}",
                    cnt + 1,
                    highScoreList[cnt].Score,
                    playTime,
                    highScoreList[cnt].ScoreDateTime));
            sb.Append(System.Environment.NewLine);
        }

        // 今回のランク表示
        if (0 < currentRank.Count())
        { sb.Append(string.Format("Your rank is {0} !", currentRank.ElementAt(0) + 1)).Append(System.Environment.NewLine); }
        else
        { sb.Append("You are out of rank!").Append(System.Environment.NewLine); }

        var rankingLabel = RankingLabelObject.GetComponent<Text>();
        rankingLabel.text = sb.ToString();

        YouDiedLabelObject.SetActive(true);
        RankingLabelObject.SetActive(true);
        GotoTitleButtonObject.SetActive(true);
    }

    /// <summary>
    /// ゲーム画面のゲージ類を更新
    /// </summary>
    private void UpdateHUD()
    {
        UpdateLabel(this.GameTimeLabelObject, string.Format("{0:0.00}", this.GameTime));
        UpdateLabel(this.ScoreLabelObject, string.Format("{0:#,0}", this.Score));
        UpdateLabel(
            this.CurrentLevelLabelObject,
            string.Format(
                "Lv. {0} (残 {1})",
                this.CurrentGameLevel,
                ((this.CurrentGameLevel * 10) - this.DefeatCount)));
        UpdateLabel(this.SpecialChargeLabelObject, string.Format("{0:#,0.0}", this._field.Player.CurrentSpecialChaege));

        UpdateLabel(this.ComboLabelObject, (0 < this.Combo ? string.Format("{0} Combo.", this.Combo) : ""));
        UpdateLabel(this.ComboRemainLabelObject, (0.0f < this.RemainComboTime ? string.Format("{0:0.00} sec.", this.RemainComboTime) : ""));

        UpdateGauge(this.LifeGaugeObject, this._field.Player.CurrentHP / this._field.Player.MaxHP);
        UpdateGauge(this.ChargeGaugeObject, this._field.Player.CurrentCharge);
    }

    /// <summary>
    /// 特殊スキルを使用するためのボタンを配置する
    /// </summary>
    private void CreateSkillButtons()
    {
        var fieldSize = this._field.FieldSize;
        var buttonWidth = fieldSize.x / 6.0f;
        var skills = this._field.Player.Skills;

        // 防御スキルボタン
        var sr = this.DefenceButton.GetComponent<SpriteRenderer>();
        sr.sprite = this._charaFactory.GetSpriteImage(skills[PlayerCharacterObject.SKILL_DEFENCE].ICON_SPRITE_NAME);
        var objectSize = this.DefenceButton.transform.localScale;
        this.DefenceButton.transform.position = new Vector3(
            -fieldSize.x + (objectSize.x * 0.5f),
            -fieldSize.y + (objectSize.y * 0.5f),
            -1.5f);

        for (int cnt = 0; cnt < this.SkillButtonList.Count; cnt++)
        {
            var skill = skills[PlayerCharacterObject.SKILL_SPECIAL + cnt];
            if (skill != null)
            {
                var skillIcon = this.SkillButtonList[cnt];

                // スキルのアイコンを設定
                var iconImage = this._charaFactory.GetSpriteImage(skill.ICON_SPRITE_NAME);
                sr = skillIcon.GetComponent<SpriteRenderer>();
                sr.sprite = iconImage;

                // ボタンの位置を設定
                objectSize = skillIcon.transform.localScale;
                skillIcon.transform.position = new Vector3(
                    buttonWidth * ((cnt * 2) - 3),
                    -fieldSize.y + (objectSize.y * 0.5f),
                    -1.5f);
            }
        }
        EnableSkillButtons(0.0f);
    }

    /// <summary>
    /// 特殊スキルボタンの表示状態を変更する
    /// </summary>
    private void EnableSkillButtons(float delta)
    {
        var fieldSize = this._field.FieldSize;
        var buttonWidth = fieldSize.x / 6.0f;

        // 防御スキル使用中は回転させる
        if (this._field.FieldObjects.Exists(x => x.GetType().Equals(typeof(DefenceSkillObject))))
        { DefenceButton.transform.Rotate(new Vector3(0.0f, 0.0f, 1800.0f * delta)); }
        else
        { DefenceButton.transform.rotation = Quaternion.identity; }

        var skills = this._field.Player.Skills;
        for (int cnt = 0; cnt < this.SkillButtonList.Count; cnt++)
        {
            var skill = skills[PlayerCharacterObject.SKILL_SPECIAL + cnt];
            if (skill != null)
            {
                var skillIcon = this.SkillButtonList[cnt];

                // ゲージが使用可能なところまで達している？
                // かつ、スキルのリキャスト完了?
                var sr = skillIcon.GetComponent<SpriteRenderer>();
                var enable = (
                    ((cnt + 1) * 100.0f <= this._field.Player.CurrentSpecialChaege) &&
                    skill.RemainRecastTime == 0.0f);
                sr.color = (enable ? Color.white : Color.gray);

                // リキャスト中の場合は回転させる
                if (0.0f < skill.RemainRecastTime)
                {
                    var rotationAngle = 360.0f * skill.RemainRecastTime * delta;
                    skillIcon.transform.Rotate(new Vector3(0.0f, 0.0f, rotationAngle));
                }
                else
                {
                    skillIcon.transform.rotation = Quaternion.identity;
                }
            }    
        }
    }

    /// <summary>
    /// ラベルの文字列を更新する
    /// </summary>
    /// <param name="o"></param>
    /// <param name="str"></param>
    private void UpdateLabel(GameObject o, string str)
    {
        o.GetComponent<Text>().text = str;
    }

    /// <summary>
    /// ゲージの長さを設定する
    /// </summary>
    /// <param name="gauge">設定対象のゲージのオブジェクト</param>
    /// <param name="currentValue">設定値。値域 [0.0f, 1.0f] で設定する</param>
    private void UpdateGauge(GameObject gauge, float currentValue)
    {
        var fieldSize = this._field.FieldSize;
        var objectPosition = gauge.transform.position;

        gauge.transform.localScale = new Vector3(fieldSize.x * 4.0f * currentValue, 1.0f);
        gauge.transform.position = new Vector3(
            -(fieldSize.x * (1.0f - currentValue)),
            objectPosition.y,
            -1.0f);
    }

    /// <summary>
    /// ゲームフィールドからキャラクタを削除する
    /// </summary>
    /// <param name="o"></param>
    public void RemoveCharacter(AbstractInFieldObject o)
    {
        Destroy(o.Character);
    }

    /// <summary>
    /// 敵を倒した
    /// </summary>
    /// <param name="bullet">敵を倒したときに使った攻撃オブジェクト</param>
    /// <param name="enemy">倒した敵</param>
    public void DefeatEnemy(
        AbstractInFieldObject bullet,
        AbstractInFieldObject enemy)
    {
        // コンボ追加
        // コンボの残り時間を設定する
        // 10コンボ未満… (11 - コンボ数 (秒))
        // 10コンボ以上… 1秒
        this.Combo++;
        this.RemainComboTime = (this.Combo < 10 ? (11.0f - (float)this.Combo) : 1.0f);

        // スコア加算
        // スコアはプレイヤーに近いほど高い
        var distance = (11.0f - enemy.Position.distance);
        if (distance < 0.0f)
        { distance = 1.0f; }

        var addScore = (ulong)((distance * 100.0f) * this.Combo);
        this.Score += addScore;

        // 特殊スキルのチャージ
        // コンボ数に応じて加算する
        // スキル使用可能になったら音を出す
        var prevGauge = this._field.Player.CurrentSpecialChaege;
        this._field.Player.CurrentSpecialChaege += this.Combo;
        if (((int)(prevGauge / 100.0f) < (int)((prevGauge + this.Combo) / 100.0f)))
        { _soundController.PlayOneshot(SoundId.GAUGE_UP); }

        // スコア表示のラベル生成
        // ラベル情報に渡す表示位置はスクリーン座標系に変換してから渡す
        var screenPos = this.MainCamera.WorldToScreenPoint(enemy.Position.ToVector3(Vector3.zero));
        this._labelQueue.Add(
            new LabelQueueStruct()
            {
                LabelString = string.Format("+{0}", addScore),
                Position = screenPos,
            });

        // 敵の撃破数を加算
        // 既定の数 (現在のレベル * 10) に達したらレベルアップ
        this.DefeatCount++;
        if((this.CurrentGameLevel * 10) <= this.DefeatCount)
        {
            this.DefeatCount -= (this.CurrentGameLevel * 10);
            this.CurrentGameLevel++;

            this._soundController.PlayOneshot(SoundId.LEVELUP);
        }
    }

    /// <summary>
    /// 画面上にラベルオブジェクトを配置する
    /// </summary>
    private void AddFieldLabel()
    {
        foreach (var s in this._labelQueue)
        {
            var scoreLabel = _charaFactory.CreateLabel(s.LabelString, s.Position);
            this._field.AddCharacter(scoreLabel);
        }
    }

    /// <summary>
    /// セーブデータからハイスコア情報をロードする
    /// </summary>
    /// <returns></returns>
    private List<HiscoreStruct> LoadHighscore()
    {
        var result = new List<HiscoreStruct>();

        var saveDataPath = System.IO.Path.Combine(UnityEngine.Application.persistentDataPath, "highscore.txt");
        var saveDataFile = new System.IO.FileInfo(saveDataPath);
        if (saveDataFile.Exists)
        {
            using (var reader = new System.IO.StreamReader(saveDataPath))
            {
                var line = reader.ReadLine();
                while(line != null)
                {
                    var values = line.Split(new char[] { '\t' });
                    var hs = new HiscoreStruct()
                    {
                        ScoreDateTime = DateTime.Parse(values[0]),
                        AliveTime = float.Parse(values[1]),
                        Score = ulong.Parse(values[2]),
                    };

                    result.Add(hs);
                    line = reader.ReadLine();
                }

                reader.Close();
            }
        }

        return result;
    }

    /// <summary>
    /// ハイスコア情報をセーブする
    /// </summary>
    /// <param name="currentTime">スコア達成時刻</param>
    /// <param name="score">今回のスコア</param>
    /// <param name="aliveTime">プレイ時間</param>
    private List<HiscoreStruct> SaveHighscore(DateTime currentTime, ulong score, float aliveTime)
    {
        // 今回のハイスコアを構造体に設定し、現在のスコア情報の末尾に追加、
        // ソートした上で上位10件を再度保存する
        var newScore = new HiscoreStruct()
        {
            ScoreDateTime = currentTime,
            AliveTime = aliveTime,
            Score = score,
        };

        var currentHighscore = LoadHighscore();
        currentHighscore.Add(newScore);
        var newHighscoreList = new List<HiscoreStruct>(currentHighscore.OrderByDescending(x => x.Score).Take(10));

        // 新しいリストの内容を保存する
        var saveDataPath = System.IO.Path.Combine(UnityEngine.Application.persistentDataPath, "highscore.txt");
        using (var writer = new System.IO.StreamWriter(saveDataPath))
        {
            for (int cnt = 0; cnt < 10; cnt++)
            {
                if (newHighscoreList.Count <= cnt)
                { break; }

                var sb = new StringBuilder();
                sb.Append(newHighscoreList[cnt].ScoreDateTime.ToString(MainSceneController.FORMAT_HISCORE_DATETIME)).Append('\t')
                    .Append(newHighscoreList[cnt].AliveTime).Append('\t')
                    .Append(newHighscoreList[cnt].Score.ToString());

                writer.WriteLine(sb.ToString());
            }
            writer.Close();
        }
        
        return newHighscoreList;
    }

    private void GotoTitleScene()
    {
        DisposeResource();
        SceneManager.LoadScene("TitleScene");
    }

    /// <summary>
    /// シーンを移動する前にこのシーンで使っていたリソースを開放する
    /// </summary>
    private void DisposeResource()
    {
        this._soundController.StopBGM();
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public MainSceneController()
    {
        this.Logger = new Logger(UnityEngine.Debug.unityLogger);
    }
}
