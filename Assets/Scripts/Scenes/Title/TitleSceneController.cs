﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Assets.Scripts.Scenes.Main;
using Assets.Scripts.Characters;
using Assets.Scripts.Skills;

public class TitleSceneController : MonoBehaviour {

    public Camera MainCamera;
    public Canvas UICanvas;

    public Text TitleLabel;
    public Button StartButton;
    public Button LicensesButton;
    public Button HiscoreButton;
    public Button EndButton;

    /// <summary>ショット選択用のボタン</summary>
    public Button ShotSelectButton;

    /// <summary>防御スキル選択用のボタン</summary>
    public Button DefenceSelectButton;

    /// <summary>スキル選択用のボタン</summary>
    public List<Button> SkillSelectButtons;

    /// <summary>
    /// キャラクタをプレハブから複製するためのファクトリオブジェクト
    /// </summary>
    public CharacterFactory _charaFactory { get; private set; }

    private AudioSource MusicAudioSource;
    private AudioClip Bgm;

    /// <summary>
    /// 選択可能なショット
    /// </summary>
    private AbstractSkill[] SelectableShots =
    {
        new NormalBulletSkill(),
        new PierceBulletSkill(),
        null,
    };

    /// <summary>
    /// 選択可能な防御スキル
    /// </summary>
    private AbstractSkill[] SelectableDefences =
    {
        new DefenceSkill(),
        null,
    };

    /// <summary>
    /// 選択可能な特殊スキル
    /// </summary>
    private AbstractSkill[] SelectabeSkills =
    {
        new HealingSKill(),
        new SlowdownSkill(),
        new EliminateSkill(),
        null,
    };

    /// <summary>
    /// 現在選択中のスキル
    /// </summary>
    private AbstractSkill[] CurrentSelectSkills;


	// Use this for initialization
	void Start () {

        this._charaFactory = new CharacterFactory() { }.Init();

        // イベントリスナ設定
        SetButtonListener(StartButton, OnClickStartButton);
        SetButtonListener(HiscoreButton, OnClickHiscoreButton);
        SetButtonListener(LicensesButton, OnClickLicensesButton);
        SetButtonListener(EndButton, OnClickEndButton);
        SetButtonListener(ShotSelectButton, OnClickShot);
        SetButtonListener(DefenceSelectButton, OnClickDefence);
        SetButtonListener(SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 0], OnClickSpecialSKill1);
        SetButtonListener(SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 1], OnClickSpecialSKill2);
        SetButtonListener(SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 2], OnClickSpecialSKill3);
        SetButtonListener(SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 3], OnClickSpecialSKill4);

        // プレイヤーのスキルの初期状態を設定
        this.CurrentSelectSkills = new AbstractSkill[]
        {
            this.SelectableShots[0],
            this.SelectableDefences[0],
            this.SelectabeSkills[0],
            this.SelectabeSkills[1],
            null,
            null,
        };

        // ボタンの配置を調整
        if (StartButton != null && LicensesButton != null && EndButton != null)
        { AlignButton(); }

        // BGM再生用AudioSourceを取得
        this.MusicAudioSource = GetComponent<AudioSource>();
        this.Bgm = (AudioClip)Resources.Load("Musics/meisou");

        // BGM再生
        this.MusicAudioSource.clip = this.Bgm;
        this.MusicAudioSource.loop = true;
        this.MusicAudioSource.Play();
    }

    // Update is called once per frame
    void Update () {
		
	}

    /// <summary>
    /// ボタンを配置する
    /// </summary>
    private void AlignButton()
    {
        var canvasRect = UICanvas.GetComponent<RectTransform>();

        // 画面中央上端にタイトル
        TitleLabel.transform.position = new Vector3(canvasRect.sizeDelta.x / 2.0f, canvasRect.sizeDelta.y * 3.0f / 4.0f, 0.0f);

        // 画面中央、下端から順番に終了、ライセンス、ハイスコア、開始
        var rt = EndButton.GetComponent<RectTransform>();

        EndButton.transform.position = new Vector3((canvasRect.sizeDelta.x + rt.sizeDelta.x) / 2.0f, rt.sizeDelta.y * 2.5f, 0.0f);
        LicensesButton.transform.position = new Vector3((canvasRect.sizeDelta.x - rt.sizeDelta.x) / 2.0f, rt.sizeDelta.y * 2.5f, 0.0f);
        HiscoreButton.transform.position = new Vector3((canvasRect.sizeDelta.x + rt.sizeDelta.x) / 2.0f, rt.sizeDelta.y * 3.5f, 0.0f);
        StartButton.transform.position = new Vector3((canvasRect.sizeDelta.x - rt.sizeDelta.x) / 2.0f, rt.sizeDelta.y * 3.5f, 0.0f);

        // スキル選択ボタン
        CreateSpecialSkillButtons(canvasRect);
    }

    /// <summary>
    /// スキル選択ボタンの初期設定
    /// </summary>
    private void CreateSpecialSkillButtons(RectTransform canvasRect)
    {
        // ショットボタンを配置
        var shotBtnRect = ShotSelectButton.GetComponent<RectTransform>();
        ShotSelectButton.transform.position = new Vector3(
            canvasRect.sizeDelta.x / 2.0f - (shotBtnRect.sizeDelta.x * 3.5f),
            canvasRect.sizeDelta.y / 2.0f, 0.0f);

        // 防御スキル選択ボタンを配置
        var defenceBtnRect = DefenceSelectButton.GetComponent<RectTransform>();
        DefenceSelectButton.transform.position = new Vector3(
            canvasRect.sizeDelta.x / 2.0f - (defenceBtnRect.sizeDelta.x * 1.5f),
            canvasRect.sizeDelta.y / 2.0f, 0.0f);

        // スキル選択ボタンを配置
        var btn = SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 0];
        var rt = btn.GetComponent<RectTransform>();
        var xPos = canvasRect.sizeDelta.x / 2.0f + (rt.sizeDelta.x * 0.5f);
        for (var cnt = 0; cnt < 4; cnt++)
        {
            SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + cnt].transform.position = new Vector3(xPos, canvasRect.sizeDelta.y / 2.0f, 0.0f);
            xPos += rt.sizeDelta.x;
        }

        // ボタンのラベル表示を更新
        UpdateSkillButton(ShotSelectButton, CurrentSelectSkills[PlayerCharacterObject.SKILL_MAINSHOT]);
        UpdateSkillButton(DefenceSelectButton, CurrentSelectSkills[PlayerCharacterObject.SKILL_DEFENCE]);
        UpdateSkillButton(SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 0], CurrentSelectSkills[PlayerCharacterObject.SKILL_SPECIAL + 0]);
        UpdateSkillButton(SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 1], CurrentSelectSkills[PlayerCharacterObject.SKILL_SPECIAL + 1]);
        UpdateSkillButton(SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 2], CurrentSelectSkills[PlayerCharacterObject.SKILL_SPECIAL + 2]);
        UpdateSkillButton(SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + 3], CurrentSelectSkills[PlayerCharacterObject.SKILL_SPECIAL + 3]);

        // スキル選択ボタンに対するガイダンス表示
        // ショットスキルのガイダンス
        var shotSkillLabel = GameObject.Find("LabelShotSkillGuidance").GetComponent<Text>();
        shotSkillLabel.transform.position = new Vector3(
            (canvasRect.sizeDelta.x / 2.0f) - (shotBtnRect.sizeDelta.x * 3.5f),
            (canvasRect.sizeDelta.y / 2.0f) + rt.sizeDelta.y,
            -1.0f);

        // 防御スキルのガイダンス
        var defenceSkillLabel = GameObject.Find("LabelDefenceSkillGuidance").GetComponent<Text>();
        defenceSkillLabel.transform.position = new Vector3(
            (canvasRect.sizeDelta.x / 2.0f) - (shotBtnRect.sizeDelta.x * 1.5f),
            (canvasRect.sizeDelta.y / 2.0f) + rt.sizeDelta.y,
            -1.0f);

        // ガイダンスの位置は6つ並べたボタンの左から4番目、5番目の間
        var skillLabel = GameObject.Find("LabelSpecialSkillGuidance").GetComponent<Text>();
        skillLabel.transform.position = new Vector3(
            (canvasRect.sizeDelta.x / 2.0f) + (shotBtnRect.sizeDelta.x * 1.0f) + rt.sizeDelta.x,
            (canvasRect.sizeDelta.y / 2.0f) + rt.sizeDelta.y,
            -1.0f);
    }

    /// <summary>
    /// 現在選択中のショットのインデックスを取得する
    /// </summary>
    /// <param name="currentSkill"></param>
    /// <returns></returns>
    private int GetCurrentShotIndex(AbstractSkill currentSkill)
    {
        for (int index = 0; index < this.SelectableShots.Length; index++)
        {
            if (currentSkill.GetType().Equals(this.SelectableShots[index].GetType()))
            { return index; }
        }
        return -1;
    }

    /// <summary>
    /// 現在選択中の特殊スキルのインデックスを取得する
    /// </summary>
    /// <param name="currentSkill"></param>
    /// <returns></returns>
    private int GetCurrentSpecialSkillIndex(AbstractSkill currentSkill)
    {
        for (int index = 0; index < this.SelectabeSkills.Length; index++)
        {
            if (currentSkill.GetType().Equals(this.SelectabeSkills[index].GetType()))
            { return index; }
        }
        return -1;
    }

    /// <summary>
    /// 指定したボタンにイベントを設定する
    /// </summary>
    /// <param name="btn">イベント設定対象のボタン</param>
    /// <param name="evt">設定するアクション</param>
    private void SetButtonListener(Button btn, UnityAction evt)
    {
        if (btn != null)
        { btn.onClick.AddListener(evt); }
    }

    /// <summary>
    /// ショット選択ボタンを押したときのイベント
    /// </summary>
    public void OnClickShot()
    {
        var currentSkillIndex = GetCurrentShotIndex(this.CurrentSelectSkills[PlayerCharacterObject.SKILL_MAINSHOT]);
        this.CurrentSelectSkills[PlayerCharacterObject.SKILL_MAINSHOT] = (this.SelectableShots[this.SelectableShots[currentSkillIndex + 1] != null ? currentSkillIndex + 1 : 0]);

        // ボタンのラベルを変更する
        UpdateSkillButton(
            this.ShotSelectButton,
            this.CurrentSelectSkills[PlayerCharacterObject.SKILL_MAINSHOT]);
    }

    /// <summary>
    /// 防御スキル選択ボタンを押したときのイベント
    /// </summary>
    public void OnClickDefence()
    {
        var currentSkillIndex = GetCurrentShotIndex(this.CurrentSelectSkills[PlayerCharacterObject.SKILL_DEFENCE]);
        this.CurrentSelectSkills[PlayerCharacterObject.SKILL_DEFENCE] = (this.SelectableDefences[this.SelectableDefences[currentSkillIndex + 1] != null ? currentSkillIndex + 1 : 0]);

        // ボタンのラベルを変更する
        UpdateSkillButton(
            this.DefenceSelectButton,
            this.SelectableDefences[PlayerCharacterObject.SKILL_DEFENCE]);
    }

    public void OnClickSpecialSKill1()
    { OnClickSpecialSkill(0); }

    public void OnClickSpecialSKill2()
    { OnClickSpecialSkill(1); }

    public void OnClickSpecialSKill3()
    { OnClickSpecialSkill(2); }

    public void OnClickSpecialSKill4()
    { OnClickSpecialSkill(3); }

    /// <summary>
    /// 特殊スキル選択ボタンを押したときのイベント
    /// </summary>
    /// <param name="index"></param>
    public void OnClickSpecialSkill(int index)
    {
        AbstractSkill nextSKill;
        if (this.CurrentSelectSkills[PlayerCharacterObject.SKILL_SPECIAL + index] == null)
        { nextSKill = this.SelectabeSkills[0]; }
        else
        {
            var currentSkillIndex = GetCurrentSpecialSkillIndex(this.CurrentSelectSkills[PlayerCharacterObject.SKILL_SPECIAL + index]);
            nextSKill = this.SelectabeSkills[currentSkillIndex + 1];
        }
        this.CurrentSelectSkills[PlayerCharacterObject.SKILL_SPECIAL + index] = nextSKill;

        // ボタンのラベルを変更する
        UpdateSkillButton(
            this.SkillSelectButtons[PlayerCharacterObject.SKILL_SPECIAL + index],
            nextSKill);
    }

    /// <summary>
    /// スキル選択ボタンのアイコンを変更する
    /// </summary>
    /// <param name="btn">アイコン変更対象のボタン</param>
    /// <param name="skill">ボタンに割り当てるスキル</param>
    public void UpdateSkillButton(
        Button btn,
        AbstractSkill skill)
    {
        btn.image.sprite = (skill != null ? _charaFactory.GetSpriteImage(skill.ICON_SPRITE_NAME) : null);
    }

    /// <summary>
    /// ゲームスタートボタンを押したときのイベント
    /// </summary>
    public void OnClickStartButton()
    {
        // ゲームメインのシーンへ遷移する
        GotoMainScene();
    }

    /// <summary>
    /// ハイスコア表示画面ボタンをイした時のイベント
    /// </summary>
    public void OnClickHiscoreButton()
    {
        DisposeResource();
        SceneManager.LoadScene("HiscoreScene");
    }

    /// <summary>
    /// ライセンス表示ボタンを押したときのイベント
    /// </summary>
    public void OnClickLicensesButton()
    {
        DisposeResource();
        SceneManager.LoadScene("LicensesScene");

    }

    /// <summary>
    /// ゲーム終了ボタンを押したときのイベント
    /// </summary>
    public void OnClickEndButton()
    {
#if UNITY_EDITOR
        // 統合環境上で実行した時はエディタに戻る
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_ANDROID
        // バックグラウンドでの動作を停止し、アプリケーションを終了する
        UnityEngine.Application.runInBackground = false;
        UnityEngine.Application.Quit();
#else
        // 単体実行の場合はアプリケーション終了
        UnityEngine.Application.Quit();
#endif
    }

    private void GotoMainScene()
    {
        // メインシーンに渡す情報
        var mainSceneArgs = new MainSceneStartParameter()
        { StartupSkills = this.CurrentSelectSkills };
        MainSceneStartParameter.Instance = mainSceneArgs;

        DisposeResource();
        SceneManager.LoadScene("MainScene");
    }

    /// <summary>
    /// シーンを移動する前にこのシーンで使っていたリソースを開放する
    /// </summary>
    private void DisposeResource()
    {
        this.MusicAudioSource.Stop();
    }
}
