﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// ライセンス表示シーンのコントローラ
/// </summary>
public class LicensesSceneController : MonoBehaviour {

    public Canvas UICanvas;
    public Button BackButton;
    public Text LicenseText;

    private AudioSource MusicAudioSource;
    private AudioClip Bgm;

    // Use this for initialization
    void Start () {

        // 戻るボタンのイベント設定
        SetButtonListener(BackButton, OnClickBackButton);

        // ボタンを配置
        AlignButton();

        // BGM再生用AudioSourceを取得
        this.MusicAudioSource = GetComponent<AudioSource>();
        this.Bgm = (AudioClip)Resources.Load("Musics/kore-ha-chikoku-ni-narimasuka");

        // BGM再生
        this.MusicAudioSource.clip = this.Bgm;
        this.MusicAudioSource.loop = true;
        this.MusicAudioSource.Play();
    }

    // Update is called once per frame
    void Update () {
		
	}

    /// <summary>
    /// ボタンを配置する
    /// </summary>
    private void AlignButton()
    {
        var canvasRect = UICanvas.GetComponent<RectTransform>();

        // 画面中央、下端に戻るボタン
        var rt = BackButton.GetComponent<RectTransform>();
        BackButton.transform.position = new Vector3(canvasRect.sizeDelta.x / 2.0f, rt.sizeDelta.y * 0.5f, -1.0f);

        // 画面上端にライセンス表示ラベル
        LicenseText.transform.position = new Vector3(canvasRect.sizeDelta.x / 2.0f, canvasRect.sizeDelta.y / 2.0f, 0.0f);
        var rtText = LicenseText.GetComponent<RectTransform>();
        var rtSize = rtText.sizeDelta;
        rtSize.x = canvasRect.sizeDelta.x * 7.0f / 8.0f;
        rtSize.y = canvasRect.sizeDelta.y * 4.0f / 5.0f;
        rtText.sizeDelta = rtSize;

        // ライセンス表示
        var textFile = Resources.Load("Strings/resources_licenses") as TextAsset;
        LicenseText.text = textFile.text;
    }

    /// <summary>
    /// 指定したボタンにイベントを設定する
    /// </summary>
    /// <param name="btn">イベント設定対象のボタン</param>
    /// <param name="evt">設定するアクション</param>
    private void SetButtonListener(Button btn, UnityAction evt)
    {
        if (btn != null)
        { btn.onClick.AddListener(evt); }
    }

    /// <summary>
    /// ゲームスタートボタンを押したときのイベント
    /// </summary>
    public void OnClickBackButton()
    {
        // タイトル画面に戻る
        BackToTitleScene();
    }

    private void BackToTitleScene()
    {
        DisposeResource();
        SceneManager.LoadScene("TitleScene");
    }

    /// <summary>
    /// シーンを移動する前にこのシーンで使っていたリソースを開放する
    /// </summary>
    private void DisposeResource()
    {
        this.MusicAudioSource.Stop();
    }
}
