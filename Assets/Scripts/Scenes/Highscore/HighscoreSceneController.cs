﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// ハイスコア表示画面のコントローラ
/// </summary>
public class HighscoreSceneController : MonoBehaviour
{
    /// <summary>
    /// ハイスコア情報
    /// </summary>
    private struct HiscoreStruct
    {
        /// <summary></summary>
        public DateTime ScoreDateTime;
        /// <summary></summary>
        public ulong Score;
        /// <summary></summary>
        public float AliveTime;
    }

    /// <summary>
    /// ハイスコア保存時の日付/時刻の書式文字列
    /// </summary>
    private static readonly string FORMAT_HISCORE_DATETIME = "yyyy/MM/dd HH:mm:ss";

    public Canvas UICanvas;
    public Button DeleteHighscoreButton;
    public Button BackButton;
    public Text ScoreNumberLabel;
    public Text ScoreLabel;
    public Text GameTimeLabel;
    public Text ScoreDateLabel;

    private AudioSource MusicAudioSource;
    private AudioClip Bgm;
    private AudioClip DeleteSound;

    // Use this for initialization
    void Start()
    {
        // ボタンのイベント設定
        SetButtonListener(BackButton, OnClickBackButton);
        SetButtonListener(DeleteHighscoreButton, OnClickDeleteHighscoreButton);

        // UIを配置
        SetUI();

        // BGM再生用AudioSourceを取得
        this.MusicAudioSource = GetComponent<AudioSource>();
        this.Bgm = (AudioClip)Resources.Load("Musics/kore-ha-chikoku-ni-narimasuka");

        // ハイスコアを削除した時の効果音を取得
        this.DeleteSound = (AudioClip)Resources.Load("Sounds/bom13");

        // BGM再生
        this.MusicAudioSource.clip = this.Bgm;
        this.MusicAudioSource.loop = true;
        this.MusicAudioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// UIを設定する
    /// </summary>
    private void SetUI()
    {
        // ボタンを配置
        AlignButton();

        // ハイスコア表示のためのラベルを配置する
        var canvasRect = UICanvas.GetComponent<RectTransform>();
        ScoreNumberLabel.transform.position = new Vector3(canvasRect.sizeDelta.x / 16.0f, canvasRect.sizeDelta.y / 2.0f, 0.0f);
        ScoreNumberLabel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, canvasRect.sizeDelta.x / 16.0f);
        ScoreNumberLabel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, canvasRect.sizeDelta.y / 2.0f);

        ScoreLabel.transform.position = new Vector3(canvasRect.sizeDelta.x * 4.0f / 16.0f, canvasRect.sizeDelta.y / 2.0f, 0.0f);
        ScoreLabel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, canvasRect.sizeDelta.x / 4.0f);
        ScoreLabel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, canvasRect.sizeDelta.y / 2.0f);

        GameTimeLabel.transform.position = new Vector3(canvasRect.sizeDelta.x * 8.0f / 16.0f, canvasRect.sizeDelta.y / 2.0f, 0.0f);
        GameTimeLabel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, canvasRect.sizeDelta.x / 4.0f);
        GameTimeLabel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, canvasRect.sizeDelta.y / 2.0f);

        ScoreDateLabel.transform.position = new Vector3(canvasRect.sizeDelta.x * 12.0f / 16.0f, canvasRect.sizeDelta.y / 2.0f, 0.0f);
        ScoreDateLabel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, canvasRect.sizeDelta.x / 4.0f);
        ScoreDateLabel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, canvasRect.sizeDelta.y / 2.0f);

        ShowHighscore();
    }

    /// <summary>
    /// ハイスコアをロードして表示する
    /// </summary>
    private void ShowHighscore()
    {
        var highScoreList = LoadHighscore();
        var sbRank = new StringBuilder();
        var sbScore = new StringBuilder();
        var sbGameTime = new StringBuilder();
        var sbDate = new StringBuilder();

        var rank = 1;
        foreach (var score in highScoreList)
        {
            sbRank.Append(rank).AppendLine();
            sbScore.Append(string.Format("{0:#,0}", score.Score)).AppendLine();

            if (score.AliveTime < 60.0f)
            {
                sbGameTime.Append(string.Format(
                    "{0:0.00}s",
                    (score.AliveTime % 60.0f)))
                    .AppendLine();
            }
            else
            {
                sbGameTime.Append(string.Format(
                    "{0,4}m {1:0.00}s",
                    (int)(score.AliveTime / 60.0f),
                    (score.AliveTime % 60.0f)))
                    .AppendLine();
            }
            sbDate.Append(string.Format("{0:yyyy/MM/dd HH:mm}", score.ScoreDateTime)).AppendLine();

            rank++;
        }

        ScoreNumberLabel.text = sbRank.ToString();
        ScoreLabel.text = sbScore.ToString();
        GameTimeLabel.text = sbGameTime.ToString();
        ScoreDateLabel.text = sbDate.ToString();
    }

    /// <summary>
    /// ボタンを配置する
    /// </summary>
    private void AlignButton()
    {
        var canvasRect = UICanvas.GetComponent<RectTransform>();

        // 画面中央、下端から戻るボタン、ハイスコア削除ボタン
        var rt = BackButton.GetComponent<RectTransform>();
        BackButton.transform.position = new Vector3(canvasRect.sizeDelta.x / 2.0f, rt.sizeDelta.y * 1.0f, -1.0f);
        DeleteHighscoreButton.transform.position = new Vector3(canvasRect.sizeDelta.x / 2.0f, rt.sizeDelta.y * 2.0f, -1.0f);
    }

    /// <summary>
    /// 指定したボタンにイベントを設定する
    /// </summary>
    /// <param name="btn">イベント設定対象のボタン</param>
    /// <param name="evt">設定するアクション</param>
    private void SetButtonListener(Button btn, UnityAction evt)
    {
        if (btn != null)
        { btn.onClick.AddListener(evt); }
    }

    /// <summary>
    /// ゲームスタートボタンを押したときのイベント
    /// </summary>
    public void OnClickBackButton()
    {
        // タイトル画面に戻る
        BackToTitleScene();
    }

    /// <summary>
    /// ハイスコア削除ボタンを押したときのイベント
    /// </summary>
    public void OnClickDeleteHighscoreButton()
    {
        // ハイスコアファイルを削除する
        var saveDataPath = System.IO.Path.Combine(UnityEngine.Application.persistentDataPath, "highscore.txt");
        var fi = new System.IO.FileInfo(saveDataPath);
        fi.Delete();
        MusicAudioSource.PlayOneShot(DeleteSound);

        ShowHighscore();
    }

    private void BackToTitleScene()
    {
        DisposeResource();
        SceneManager.LoadScene("TitleScene");
    }

    /// <summary>
    /// シーンを移動する前にこのシーンで使っていたリソースを開放する
    /// </summary>
    private void DisposeResource()
    {
        this.MusicAudioSource.Stop();
    }

    /// <summary>
    /// セーブデータからハイスコア情報をロードする
    /// </summary>
    /// <returns></returns>
    private List<HiscoreStruct> LoadHighscore()
    {
        var result = new List<HiscoreStruct>();

        var saveDataPath = System.IO.Path.Combine(UnityEngine.Application.persistentDataPath, "highscore.txt");
        var saveDataFile = new System.IO.FileInfo(saveDataPath);
        if (saveDataFile.Exists)
        {
            using (var reader = new System.IO.StreamReader(saveDataPath))
            {
                var line = reader.ReadLine();
                while (line != null)
                {
                    var values = line.Split(new char[] { '\t' });
                    var hs = new HiscoreStruct()
                    {
                        ScoreDateTime = DateTime.Parse(values[0]),
                        AliveTime = float.Parse(values[1]),
                        Score = ulong.Parse(values[2]),
                    };

                    result.Add(hs);
                    line = reader.ReadLine();
                }
                reader.Close();
            }
        }
        return result;
    }

}
